# Start from node base image
FROM node:12-alpine

# Set the current working directory inside the container
WORKDIR /home/phenom-commons

# Add nx cli
RUN yarn global add @nrwl/cli

RUN apk add --no-cache python py-pip
RUN pip install awscli

# Copy package.json, yarn.lock files and download deps
COPY package.json yarn.lock ./
RUN yarn

COPY . .

# Set the node environment
ARG node_env=production
ENV NODE_ENV $node_env

# Build
ARG project
RUN nx build $project --with-deps
