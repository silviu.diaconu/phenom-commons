module.exports = {
  displayName: 'emails',
  preset: '../../jest.preset.js',
  globals: {
    'ts-jest': {
      tsConfig: '<rootDir>/tsconfig.spec.json',
    },
  },
  transform: {
    '^.+\\.[tj]sx?$': 'ts-jest',
  },
  moduleFileExtensions: ['ts', 'tsx', 'js', 'jsx'],
  coverageDirectory: '../../coverage/libs/emails',
  setupFiles: ["<rootDir>/jest/setJestEnv.ts"],
  testEnvironment: 'node'
};
