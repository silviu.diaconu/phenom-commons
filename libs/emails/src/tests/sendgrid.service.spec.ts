import { Test, TestingModule } from '@nestjs/testing';
import fs, { readFileSync } from 'fs';
import { EmailsFactory } from '@hindawi/emails';
import { EmailOptions, EmailResponse, EmailService } from '../lib/emails.contract';

describe('SendgridService', () => {
  let moduleRef: TestingModule;
  let service: EmailService;
  const SENDGRID_TOKEN = 'SendgridEmailService';

  beforeAll(async () => {
    moduleRef = await Test.createTestingModule({
      imports: [],
      providers: [
        {
          provide: SENDGRID_TOKEN,
          useFactory: () => {
            return EmailsFactory.sendgrid(process.env.SENDGRID_API_SECRET, true);
          },
        },
      ],
    }).compile();

    service = await moduleRef.get<EmailService>(SENDGRID_TOKEN);
  });

  const defaults = {
    to: 'my@email.com',
    from: 'my@email.com',
    subject: 'random',
    html: '<strong>some bold text</strong>',
  } as EmailOptions;

  describe('sendEmail', () => {
    it('should send simple email', async () => {
      const inputs = [
        { ...defaults, to: 'my@email.com' },
        { ...defaults, to: { email: 'my@email.com' } },
      ];
      await verifySuccess(inputs, service);
    });

    it('should send email to multiple addresses', async () => {
      const inputs = [
        { ...defaults, to: ['my@email.com', 'random.alice@example.com'] },
        {
          ...defaults,
          to: [{ email: 'my@email.com' }, { email: 'random.alice@example.com' }],
        },
      ];
      await verifySuccess(inputs, service);
    });

    it('should send email with cc and bcc', async () => {
      const inputs = [
        {
          ...defaults,
          to: 'my@email.com',
          cc: 'random.alice@example.com',
          bcc: 'random.joe@example.com',
        },
        {
          ...defaults,
          to: 'my@email.com',
          cc: ['random.alice@example.com', 'random.tony@example.com'],
          bcc: ['random.joe@example.com', 'random.jack@example.com'],
        },
        {
          ...defaults,
          to: 'my@email.com',
          cc: [
            {
              email: 'random.alice@example.com',
              name: 'John Doe',
            },
            'random.tony@example.com',
          ],
          bcc: ['random.joe@example.com', 'random.jack@example.com'],
        },
      ];
      await verifySuccess(inputs, service);
    });
    it('should send email with replyTo', async () => {
      const inputs = [
        {
          ...defaults,
          replyTo: {
            email: 'replyto@example.com',
            name: 'Reply To',
          },
        },
      ] as EmailOptions[];

      await verifySuccess(inputs, service);
    });

    it('should send mail with attachments', async () => {
      const sampleFile = fs
        .readFileSync('./libs/emails/src/tests/test-files/' + 'sample.pdf')
        .toString('base64');

      const inputs = [
        {
          ...defaults,
          attachments: [{ content: sampleFile, filename: 'sample.pdf', type: 'application/pdf' }],
        },
        {
          ...defaults,
          attachments: [
            { content: sampleFile, filename: 'sample.pdf', type: 'application/pdf' },
            { content: sampleFile, filename: 'sample.pdf', type: 'application/pdf' },
          ],
        },
      ];
      await verifySuccess(inputs, service);
    });

    it('should fail', async () => {
      const inputs = [
        {},
        { ...defaults, to: undefined },
        { ...defaults, to: 'same@email.com', cc: 'same@email.com' },
        { ...defaults, to: 'same@email.com', bcc: 'same@email.com' },
      ];
      await verifyFail(inputs, service);
    });
  });
});

function expectSuccess(response: EmailResponse) {
  expect(response.success).toBe(true);
  expect(response.statusCode === '200' || response.statusCode === '202');
}

function expectFail(response: EmailResponse) {
  expect(response.success).toBe(false);
  expect(response.statusCode !== '200' && response.statusCode !== '202');
  expect(response.messages.length).toBeGreaterThan(0);
  console.log(response.messages);
}

async function verifySuccess(inputs: EmailOptions[], service: EmailService) {
  for (const input of inputs) {
    const response = await service.send(input);
    console.log(response);
    expectSuccess(response);
  }
}

async function verifyFail(inputs, service: EmailService) {
  for (const input of inputs) {
    const response = await service.send(input);
    console.log(response);
    expectFail(response);
  }
}
