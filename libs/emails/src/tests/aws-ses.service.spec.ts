import { Test, TestingModule } from '@nestjs/testing';
import { EmailService } from '../lib/emails.contract';
import { EmailsFactory } from '@hindawi/emails';

describe('AwsSesService', () => {
  let moduleRef: TestingModule;
  let service: EmailService;
  const AWS_SES_TOKEN = 'AwsSesEmailService';

  beforeAll(async () => {
    moduleRef = await Test.createTestingModule({
      imports: [],
      providers: [
        {
          provide: AWS_SES_TOKEN,
          useFactory: () => {
            return EmailsFactory.awsSes({
              accessKeyId: process.env.accessKeyId,
              secretAccessKey: process.env.secretAccessKey,
              region: process.env.sesregion,
            });
          },
        },
      ],
    }).compile();

    service = await moduleRef.get<EmailService>(AWS_SES_TOKEN);
  });

  describe('sendEmail', () => {
    it('should send simple email', async () => {
      const res = await service.send({
        from: 'ioana.abacioaiei@hindawi.com',
        to: 'ioana.abacioaiei@hindawi.com',
        subject: 'random',
        text: 'email body',
      });
      expect(res).toMatchObject({ success: true, statusCode: '200', messages: [] });
    });

    it('should send email with cc and bcc', async () => {
      const res = await service.send({
        from: 'ioana.abacioaiei@hindawi.com',
        to: 'ioana.abacioaiei@hindawi.com',
        cc: 'ioana.abacioaiei@hindawi.com',
        bcc: 'ioana.abacioaiei@hindawi.com',
        subject: 'random',
        html: '<strong>some bold text</strong>',
      });
      expect(res).toMatchObject({ success: true, statusCode: '200', messages: [] });
    });

    it('should send email with multiple cc and bcc', async () => {
      const res = await service.send({
        from: 'ioana.abacioaiei@hindawi.com',
        to: 'ioana.abacioaiei@hindawi.com',
        cc: 'ioana.abacioaiei@hindawi.com',
        bcc: 'ioana.abacioaiei@hindawi.com',
        subject: 'random',
        html: '<strong>some bold text</strong>',
      });
      expect(res).toMatchObject({ success: true, statusCode: '200', messages: [] });
    });

    it('should throw error Invalid email address for sender', async () => {
      const res = await service.send({
        from: '',
        to: 'ioana.abacioaiei@hindawi.com',
        cc: 'ioana.abacioaiei@hindawi.com',
        bcc: 'ioana.abacioaiei@hindawi.com',
        subject: 'random',
        html: '<strong>some bold text</strong>',
      });
      expect(res).toMatchObject({
        success: false,
        statusCode: '400',
        messages: 'Invalid email address .',
      });
    });

    it('should throw error Invalid email address for receiver', async () => {
      const res = await service.send({
        from: 'ioana.abacioaiei@hindawi.com',
        to: '',
        cc: 'ioana.abacioaiei@hindawi.com',
        bcc: 'ioana.abacioaiei@hindawi.com',
        subject: 'random',
        html: '<strong>some bold text</strong>',
      });
      expect(res).toMatchObject({
        success: false,
        statusCode: '400',
        messages: 'Invalid email address .',
      });
    });
  });
});
