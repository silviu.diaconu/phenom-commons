import { SendgridService } from './sendgrid.service';
import { AwsSesService } from './aws-ses.sevice';
import { ClientConfiguration } from 'aws-sdk/clients/s3';
import { EmailService } from './emails.contract';

export class EmailsFactory {
  static sendgrid(apiKey: string, sandboxMode: boolean): EmailService {
    return new SendgridService(apiKey, sandboxMode);
  }

  static awsSes(sesConfig: ClientConfiguration): EmailService {
    return new AwsSesService(sesConfig);
  }
}
