import AWS from 'aws-sdk';
import { SendEmailRequest } from 'aws-sdk/clients/ses';
import { ClientConfiguration } from 'aws-sdk/clients/s3';
import { EmailOptions, EmailResponse, EmailService } from '@hindawi/emails';

export class AwsSesService implements EmailService {
  ses: AWS.SES;
  sesConfig: ClientConfiguration;

  constructor(sesConfig: ClientConfiguration) {
    this.sesConfig = sesConfig;

    this.ses = new AWS.SES({
      secretAccessKey: sesConfig.secretAccessKey,
      accessKeyId: sesConfig.accessKeyId,
      region: sesConfig.region,
    });
  }

  async send({ from, to, cc, bcc, subject, text, html }: EmailOptions): Promise<EmailResponse> {
    const emailResponse: EmailResponse = { success: true, statusCode: '200', messages: [] };
    const msg: SendEmailRequest = {
      Source: from as string,
      Destination: {
        ToAddresses: [to] as [string],
        BccAddresses: [bcc] as [string],
        CcAddresses: [cc] as [string],
      },
      ReplyToAddresses: [],
      Message: {
        Body: {
          Html: {
            Charset: 'UTF-8',
            Data: html || '',
          },
          Text: {
            Charset: 'UTF-8',
            Data: text || '',
          },
        },
        Subject: {
          Charset: 'UTF-8',
          Data: subject,
        },
      },
    };

    try {
      const response = await this.ses.sendEmail(msg).promise();
    } catch (error) {
      console.log(error);
      emailResponse.messages = error?.message || ['Unknown error'];
      emailResponse.statusCode = error?.statusCode?.toString() || 'Unknown status code';
      emailResponse.success = false;
    }

    return emailResponse;
  }
}
