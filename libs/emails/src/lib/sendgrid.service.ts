import ssg from '@sendgrid/mail';
import { EmailOptions, EmailResponse, EmailService } from '@hindawi/emails';


export class SendgridService implements EmailService {
  private readonly sandboxMode: boolean;

  constructor(apiKey: string, sandboxMode = true) {
    this.sandboxMode = sandboxMode;
    ssg.setApiKey(apiKey);
  }

  async send({ from, to, cc, bcc, subject, text, html, attachments, replyTo }: EmailOptions): Promise<EmailResponse> {

    const mailAttachments = attachments ? attachments.map(attachmentData => ({
      content: attachmentData.content,
      filename: attachmentData.filename,
      type: attachmentData.filename,
      disposition: 'attachment',
    })) : undefined;

    let sentEmail;
    try {
      sentEmail = await ssg.send({
        from: from,
        to: to,
        cc: cc,
        bcc: bcc,
        subject: subject,
        text: text,
        html: html,
        attachments: mailAttachments,
        replyTo: replyTo,
        mailSettings: {
          sandboxMode: {
            enable: this.sandboxMode,
          },
        },
      }, false);

      return {
        success: true,
        statusCode: sentEmail[0].statusCode.toString(),
      } as EmailResponse;

    } catch (e) {
      const messages = [e.message];
      if (e.response && e.response.body && e.response.body.errors) {
        messages.push(...e.response.body.errors.map(error => error.message));
      }
      return {
        success: false,
        statusCode: e.code,
        messages: messages,
      } as EmailResponse;
    }
  }
}
