export type EmailData = string | { name?: string; email: string };

export type EmailResponse = {
  success: boolean;
  statusCode?: string;
  messages?: string[];
};

export type AttachmentData = {
  content: string; //base64
  filename: string;
  type?: string;
};

export type EmailOptions = {
  from: EmailData;
  to?: EmailData | EmailData[];
  cc?: EmailData | EmailData[];
  bcc?: EmailData | EmailData[];
  subject: string;
  text?: string;
  html?: string;
  replyTo?: EmailData;
  attachments?: AttachmentData[];
};

export interface EmailService {
  send(options: EmailOptions): Promise<EmailResponse>;
}
