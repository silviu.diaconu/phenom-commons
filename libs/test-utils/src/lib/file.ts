import Chance from 'chance';
import { PhenomFile } from '@hindawi/phenom-events/src/lib/phenomFile';

const chance = new Chance();

export function generateFileTypes() {
  return types;
}

const types = {
  figure: 'figure',
  manuscript: 'manuscript',
  coverLetter: 'coverLetter',
  supplementary: 'supplementary',
  reviewComment: 'reviewComment',
  responseToReviewers: 'responseToReviewers'
};
