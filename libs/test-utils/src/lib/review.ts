import Chance from 'chance';

const chance = new Chance();


export function getRecommendations() {
  return recommendations;
}


const recommendations = {
  responseToRevision: 'responseToRevision',
  minor: 'minor',
  major: 'major',
  reject: 'reject',
  publish: 'publish',
  revision: 'revision',
  returnToAcademicEditor: 'returnToAcademicEditor'
};
