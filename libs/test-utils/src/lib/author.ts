import { Author } from '@hindawi/phenom-events/src/lib/author';
import Chance from 'chance';
import { PhenomFile } from '@hindawi/phenom-events/src/lib/phenomFile';
import { Editor } from '@hindawi/phenom-events/src/lib/editor';
import { Review } from '@hindawi/phenom-events/src/lib/review';
import { EventObject } from '@hindawi/phenom-events/src/lib/eventObject';

//todo remove this
interface Comment extends EventObject {
  content: string;
  files: PhenomFile[];
  type: string;
}

const chance = new Chance();

export function generateComment(props?: Partial<Comment>): Comment {
  return {
    content: chance.paragraph(),
    created: '',
    files: [],
    id: '',
    type: '',
    updated: '',
    ...props,
  };
}

export function generateReview(props?): Review {
  return {
    id: chance.guid(),
    isValid: chance.bool(),
    comments: [generateComment()],
    recommendation: chance.text(),
    reviewerId: chance.guid(),
    ...props,
  };
}


export function generateEditor(props?: Partial<Editor>): Editor {
  return {
    acceptedDate: chance.date().toString(),
    assignedDate: chance.date().toString(),
    declinedDate: chance.date().toString(),
    email: chance.email(),
    expiredDate: chance.date().toString(),
    givenNames: chance.name(),
    id: chance.guid(),
    invitedDate: chance.date().toString(),
    orcidId: chance.guid(),
    removedDate: chance.date().toString(),
    role: {
      type: '',
      label: '',
    },
    status: undefined,
    surname: '',
    userId: '',
    ...props,
  };
}

export function generateFile(props?: Partial<PhenomFile> | any): PhenomFile {
  return {
    mimeType: '',
    providerKey: '',
    size: 0,
    updated: '',
    id: chance.guid(),
    created: chance.date(),
    type: chance.string(),
    fileName: chance.name(),
    originalName: chance.name(),
    ...props,
  };
}


export function generateAuthor(props?): Author {
  return {
    id: chance.guid(),
    created: '2020-10-05T13:06:17.375Z',
    updated: '2020-10-05T13:06:17.375Z',
    userId: chance.guid(),
    status: chance.pickone(['pending', 'accepted']),
    position: 0,
    isSubmitting: chance.bool(),
    isCorresponding: chance.bool(),
    aff: 'HINDAWI',
    email: chance.email(),
    title: chance.pickone(['mr', 'ms']),
    country: chance.country(),
    surname: chance.name(),
    givenNames: chance.name(),
    orcidId: chance.guid(),
    assignedDate: '2020-10-05T13:06:17.375Z',
    ...props,
  } as Author;

}
