import Chance from 'chance';

const chance = new Chance();

export function generateJournal(props) {
  return {
    id: chance.guid(),
    name: chance.name(),
    save: jest.fn(),
    updateProperties: jest.fn(),
    saveGraph: jest.fn(),
    scheduleJob: jest.fn(),
    cancelJobs: jest.fn(),
    ...props
  };
}
