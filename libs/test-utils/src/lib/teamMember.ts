import Chance from 'chance';
import { Author } from '@hindawi/phenom-events/src/lib/author';

const chance = new Chance();
const statuses = {
  pending: 'pending',
  accepted: 'accepted',
  declined: 'declined',
  submitted: 'submitted',
  expired: 'expired',
  removed: 'removed',
  active: 'active',
};
const statusExpiredLabels = {
  overdue: 'OVERDUE',
  unresponsive: 'UNRESPONSIVE',
  unsubscribed: 'UNSUBSCRIBED',
};



export function getTeamMemberStatuses() {
  return statuses;
};

export function getStatusExpiredLabels() {
  return statusExpiredLabels;
}
