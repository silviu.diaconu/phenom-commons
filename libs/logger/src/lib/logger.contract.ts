export type EnvOptions = {
  env: string;
  applicationName: string;
}

export interface HindawiLogger {
  appendDefaultMeta(key: string, value: string): void;
  /**
   * @param context - Use classname in most cases
   */
  setContext(context: string): void;
  error(message: any, trace?: string): void;
  info(message: any): void;
  log(message: any, level?: string): void;
  warn(message: any): void;
  debug(message: any): void;
  verbose(message: any): void;
}
