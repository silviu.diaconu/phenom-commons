import { DynamicModule, Global, Module } from '@nestjs/common';
import { WinstonLoggerService } from '@hindawi/logger';
import { format, LoggerOptions, transports } from 'winston';
import { LOGGER_ENV_OPTIONS, WINSTON_CONFIG_OPTIONS } from './logger.constants';
import { LoggingInterceptor } from '@hindawi/logger';
import { EnvOptions } from '@hindawi/logger';
import { APP_INTERCEPTOR } from '@nestjs/core';

@Global()
@Module({})
export class LoggerModule {
  static winston(envOptions: EnvOptions, options?: LoggerOptions): DynamicModule {
    options = options || {
      level: process.env.LOG_LEVEL || 'debug',
      format: format.combine(
        format.errors({ stack: true }),
        format.timestamp({ format: 'isoDateTime' }),
        format.json(),
        format.timestamp(),
        format.prettyPrint(),
        format.colorize({ all: true }),
      ),
      transports: [
        // new transports.File({ filename: 'error.log', level: 'error' }),
        // new transports.File({ filename: 'combined.log' }),
        new transports.Console(),
      ],
    };

    return {
      module: LoggerModule,
      providers: [
        WinstonLoggerService,
        LoggingInterceptor,
        {
          provide: WINSTON_CONFIG_OPTIONS,
          useValue: options,
        },
        {
          provide: LOGGER_ENV_OPTIONS,
          useValue: envOptions,
        },
        {
          provide: APP_INTERCEPTOR,
          useClass: LoggingInterceptor,
        },
      ],
      exports: [WinstonLoggerService, LoggingInterceptor],
    };
  }
}
