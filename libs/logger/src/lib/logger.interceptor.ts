import {
  Injectable,
  NestInterceptor,
  ExecutionContext,
  CallHandler,
  Inject,
} from '@nestjs/common';
import { GqlContextType, GqlExecutionContext } from '@nestjs/graphql';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { Request } from 'express';
import { WinstonLoggerService } from '@hindawi/logger';
import { v4 as uuid } from 'uuid';
import { HindawiLogger } from '@hindawi/logger';

export const LOG_TYPE = {
  REQUEST_ARGS: 'Request args',
  RESPONSE_RESULT: 'Response result',
};

@Injectable()
export class LoggingInterceptor implements NestInterceptor {
  constructor(
    @Inject(WinstonLoggerService)
    private readonly logger: HindawiLogger,
  ) {}

  intercept(executionCtx: ExecutionContext, next: CallHandler): Observable<any> {
    const requestId = uuid();
    this.logger.appendDefaultMeta('request_id', requestId);
    this.logger.setContext(executionCtx.getClass().name);

    this.logRequest(executionCtx, 'debug');

    const now = Date.now();
    return next.handle().pipe(
      tap({
        next: (value) => {
          //print response
          this.logger.debug(`${JSON.stringify({ Response: value })}`);
        },
        error: (err) => {
          this.logger.error(err.message, err.stack);
          this.logRequest(executionCtx, 'error');
        },
        complete: () =>
          this.logger.info(`${executionCtx.getType()} finished in ... ${Date.now() - now} ms`),
      }),
    );
  }

  private logRequest(executionCtx: ExecutionContext, level: string) {
    if (executionCtx.getType() === 'http') {
      this.logHttpRequests(executionCtx);
    } else if (executionCtx.getType() === 'rpc') {
      this.logRpcRequests(executionCtx, level);
    } else if (executionCtx.getType<GqlContextType>() === 'graphql') {
      this.logGraphqlRequests(executionCtx, level);
    }
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  private logHttpRequests(executionCtx: ExecutionContext) {
    // const ctx = executionCtx.switchToHttp();
    // const request = ctx.getRequest<Request>();
    // do something
  }

  private logGraphqlRequests(executionCtx: ExecutionContext, logLevel: string) {
    const httpCtx = executionCtx.switchToHttp();
    const gqlContext = GqlExecutionContext.create(executionCtx);
    const args = gqlContext.getArgs();

    this.logger.log(
      `${JSON.stringify({
        headers: httpCtx.getRequest<Request>()?.headers,
        type: LOG_TYPE.REQUEST_ARGS,
        value: args,
      })}`,
      logLevel,
    );
  }

  private logRpcRequests(executionCtx: ExecutionContext, level: string) {
    const rpcCtx = executionCtx.switchToRpc();

    const handlerFunction = Reflect.get(rpcCtx, 'handler');

    if (handlerFunction) {
      this.logger.log(
        `${JSON.stringify({
          handler: handlerFunction.name,
          data: rpcCtx.getData(),
        })}`,
        level,
      );
    }
  }
}
