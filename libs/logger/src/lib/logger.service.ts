import { Inject, Injectable, Scope } from '@nestjs/common';
import winston, { createLogger, LoggerOptions } from 'winston';
import { LOGGER_ENV_OPTIONS, WINSTON_CONFIG_OPTIONS } from './logger.constants';
import { EnvOptions, HindawiLogger } from './logger.contract';

@Injectable({ scope: Scope.TRANSIENT })
export class WinstonLoggerService implements HindawiLogger {
  private readonly winstonLogger: winston.Logger;

  constructor(
    @Inject(WINSTON_CONFIG_OPTIONS) config: LoggerOptions,
    @Inject(LOGGER_ENV_OPTIONS) envOptions: EnvOptions,
  ) {
    this.winstonLogger = createLogger(config);
    this.appendDefaultMeta('env', envOptions.env);
    this.appendDefaultMeta('application_name', envOptions.applicationName);
  }

  setContext(serviceName: string) {
    this.winstonLogger.defaultMeta = {
      ...this.winstonLogger.defaultMeta,
      service_class: serviceName,
    };
  }

  appendDefaultMeta(key: string, value: string) {
    this.winstonLogger.defaultMeta = {
      ...this.winstonLogger.defaultMeta,
      [key]: value,
    };
  }

  info(message: string) {
    this.winstonLogger.info(message);
  }

  log(message: string, level?: string) {
    this.winstonLogger.log(level || 'debug', message, this.winstonLogger.defaultMeta);
  }

  error(message: string, trace?: string) {
    this.winstonLogger.error(message, {
      trace: trace,
    });
  }
  warn(message: string) {
    this.winstonLogger.warn(message);
  }
  debug(message: string) {
    this.winstonLogger.debug(message);
  }
  verbose(message: string) {
    this.winstonLogger.verbose(message);
  }
}
