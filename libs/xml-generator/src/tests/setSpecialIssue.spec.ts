import { setSpecialIssue } from '../lib/setManuscriptData/setSpecialIssue';

describe('setSpecialIssue setter', () => {
  it('returns an empty object if no special issue id or name is provided', () => {
    const specialIssueId = undefined
    const specialIssueName = undefined

    const res = setSpecialIssue({ specialIssueId, specialIssueName })

    expect(res).toEqual({})
  })
  it('returns a correct object', () => {
    const specialIssueId = '123456'
    const specialIssueName = 'some-title'

    const res = setSpecialIssue({ specialIssueId, specialIssueName })

    expect(res.article.front['article-meta']['special-issue-id']).toEqual({
      _text: specialIssueId,
    })
    expect(res.article.front['article-meta']['special-issue-title']).toEqual({
      _text: specialIssueName,
    })
  })
})
