import { setAcademicEditorContributor } from '../lib/setManuscriptData/setContributors/setAcademicEditorContributor';
// eslint-disable-next-line @nrwl/nx/enforce-module-boundaries
import { generateEditor } from '@phenom-commons/test-utils';

describe('setAcademicEditorContributor setter', () => {

  it('returns a correct object if the user has no orcid identity', () => {
    const academicEditor = generateEditor({ orcidId: undefined });

    const res = setAcademicEditorContributor(academicEditor);

    expect(res.article.front['article-meta']['contrib-group'].contrib).toEqual([
      {
        _attributes: { 'contrib-type': 'Academic Editor' },
        role: { _attributes: { 'content-type': '1' } },
        name: {
          surname: { _text: academicEditor.surname },
          'given-names': { _text: academicEditor.givenNames },
          prefix: { _text: academicEditor.title },
        },
        email: { _text: academicEditor.email },
        xref: {
          _attributes: {
            'ref-type': 'aff',
            rid: `I1`,
          },
          sup: {
            _text: 1,
          },
        },
      },
    ]);

    expect(res.article.front['article-meta'].aff).toEqual([
      {
        _attributes: {
          id: `I1`,
        },
        sup: {
          _text: 1,
        },
        'addr-line': { _text: academicEditor.aff || '' },
        country: academicEditor.country || 'UK',
      },
    ]);
  });
  it('returns a correct object', () => {
    const identity = 'klaus';
    const academicEditor = generateEditor({
      orcidId: identity,
    });

    const res = setAcademicEditorContributor(academicEditor);

    expect(res.article.front['article-meta']['contrib-group'].contrib).toEqual([
      {
        _attributes: { 'contrib-type': 'Academic Editor' },
        'contrib-id': {
          _attributes: { 'contrib-id-type': 'orcid' },
          _text: `https://orcid.org/${identity}`,
        },
        role: { _attributes: { 'content-type': '1' } },
        name: {
          surname: { _text: academicEditor.surname },
          'given-names': { _text: academicEditor.givenNames },
          prefix: { _text: academicEditor.title },
        },
        email: { _text: academicEditor.email },
        xref: {
          _attributes: {
            'ref-type': 'aff',
            rid: `I1`,
          },
          sup: {
            _text: 1,
          },
        },
      },
    ]);

    expect(res.article.front['article-meta'].aff).toEqual([
      {
        _attributes: {
          id: `I1`,
        },
        sup: {
          _text: 1,
        },
        'addr-line': { _text: academicEditor.aff || '' },
        country: academicEditor.country || 'UK',
      },
    ]);
  });
});
