import { setScreenerContacts } from '../lib/setContacts/setScreenerContacts';


describe('setScreenerContacts setter', () => {
  it('returns an empty object if there are no screeners', () => {
    const screeners = undefined

    const res = setScreenerContacts({ screeners })

    expect(res).toEqual({})
  })
  it('returns an empty object if the screeners array is empty', () => {
    const screeners = []

    const res = setScreenerContacts({ screeners })

    expect(res).toEqual({})
  })
  it('returns a correct object', () => {
    const screener = {
      role: 'qc',
      email: 'some-email',
      surname: 'some-surname',
      givenNames: 'some-given-name',
    }
    const screeners = [screener]

    const res = setScreenerContacts({ screeners })

    expect(res.article.front.contacts['contact-person']).toEqual([
      {
        _attributes: { role: 'QC' },
        name: {
          surname: { _text: screener.surname },
          'given-names': {
            _text: screener.givenNames,
          },
        },
        email: { _text: screener.email },
      },
    ])
  })
})
