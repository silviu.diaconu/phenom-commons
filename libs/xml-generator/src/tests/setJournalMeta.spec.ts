import { setJournalMeta } from '../lib/setJournalMeta';
// eslint-disable-next-line @nrwl/nx/enforce-module-boundaries
import { generateJournal } from '@phenom-commons/test-utils';

describe('setJournalMeta setter', () => {
  it('returns a correct object', () => {
    const journal = generateJournal({
      code: 'some-cod',
      email: 'some-email',
      issn: 'some-issn',
    })

    const res = setJournalMeta(journal)

    expect(res.article.front['journal-meta']).toEqual({
      'journal-id': [
        {
          _attributes: { 'journal-id-type': 'publisher' },
          _text: journal.code,
        },
        {
          _attributes: { 'journal-id-type': 'email' },
          _text: journal.email,
        },
      ],
      'journal-subcode': { _text: journal.code },
      'journal-title-group': {
        'journal-title': { _text: journal.name },
      },
      issn: [
        {
          _attributes: { 'pub-type': 'epub' },
          _text: journal.issn,
        },
      ],
      publisher: { 'publisher-name': { _text: 'Hindawi' } },
    })
  })
})
