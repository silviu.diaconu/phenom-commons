import { setEditorialAssistantContact } from '../lib/setContacts/setEditorialAssistantContact';
// eslint-disable-next-line @nrwl/nx/enforce-module-boundaries
import { generateEditor } from '@phenom-commons/test-utils';


describe('setEditorialAssistantContact setter', () => {
  it('returns an empty object if there is no editorial assistant', () => {
    const editorialAssistant = undefined

    const res = setEditorialAssistantContact({
      editorialAssistant,
    })

    expect(res).toEqual({})
  })
  it('returns a correct object', () => {
    const editorialAssistant = generateEditor()

    const res = setEditorialAssistantContact({
      editorialAssistant,
    })

    expect(res.article.front.contacts['contact-person']).toEqual([
      {
        _attributes: { role: 'EA' },
        name: {
          surname: { _text: editorialAssistant.surname },
          'given-names': {
            _text: editorialAssistant.givenNames,
          },
        },
        email: { _text: editorialAssistant.email },
      },
    ])
  })
})
