import { setVersion } from '../lib/setManuscriptData/setVersion';

describe('setVersion setter', () => {
  it('returns a correct object', () => {
    const version = '1.2'

    const res = setVersion({ version })

    expect(res.article.front['article-meta']['article-version']).toEqual({
      _text: version,
    })
  })
})
