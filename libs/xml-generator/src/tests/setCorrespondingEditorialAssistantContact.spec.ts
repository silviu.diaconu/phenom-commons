import { setCorrespondingEditorialAssistantContact } from '../lib/setContacts/setCorrespondingEditorialAssistantContact';
// eslint-disable-next-line @nrwl/nx/enforce-module-boundaries
import { generateEditor } from '@phenom-commons/test-utils';

describe('setCorrespondingEditorialAssistantContact setter', () => {
  it('returns an empty object if there is no editorial assistant', () => {
    const correspondingEditorialAssistant = undefined

    const res = setCorrespondingEditorialAssistantContact({
      correspondingEditorialAssistant,
    })

    expect(res).toEqual({})
  })
  it('returns a correct object', () => {
    const correspondingEditorialAssistant = generateEditor()

    const res = setCorrespondingEditorialAssistantContact({
      correspondingEditorialAssistant,
    })

    expect(res.article.front.contacts['contact-person']).toEqual([
      {
        _attributes: { role: 'EALeader' },
        name: {
          surname: { _text: correspondingEditorialAssistant.surname },
          'given-names': {
            _text: correspondingEditorialAssistant.givenNames,
          },
        },
        email: { _text: correspondingEditorialAssistant.email },
      },
    ])
  })
})
