import { setAuthorContributors } from '../lib/setManuscriptData/setContributors/setAuthorContributors';
// eslint-disable-next-line @nrwl/nx/enforce-module-boundaries
import { generateAuthor } from '@phenom-commons/test-utils';


describe('setAuthorContributors setter', () => {
  it('throws an error if the user is not loaded', () => {
    const author = generateAuthor({ user: undefined });

    try {
      setAuthorContributors([author]);
    } catch (err) {
      expect(err.message).toBe(
        `Author ${author.id} does not have a loaded user`,
      );
    }
  });
  it('throws an error if the user identities are not loaded', () => {
    const author = generateAuthor();

    try {
      setAuthorContributors([author]);
    } catch (err) {
      expect(err.message).toBe(
        `Author ${author.id} does not have loaded identities`,
      );
    }
  });
  it('returns a correct object if the user has no orcid identity', () => {
    const author = generateAuthor({
      orcidId: undefined,
    });

    const res = setAuthorContributors([author]);

    expect(res.article.front['article-meta']['contrib-group'].contrib).toEqual([
      {
        _attributes: {
          'contrib-type': 'Author',
          corresp: author.isCorresponding ? 'Yes' : 'No',
          submitting: author.isSubmitting ? 'Yes' : 'No',
        },
        role: { _attributes: { 'content-type': '1' } },
        name: {
          surname: { _text: author.surname },
          'given-names': { _text: author.givenNames },
          prefix: { _text: author.title },
        },
        email: { _text: author.email },
        xref: {
          _attributes: {
            'ref-type': 'aff',
            rid: `I2`,
          },
          sup: {
            _text: 2,
          },
        },
      },
    ]);

    expect(res.article.front['article-meta'].aff).toEqual([
      {
        _attributes: {
          id: `I2`,
        },
        sup: {
          _text: 2,
        },
        'addr-line': { _text: author.aff || '' },
        country: author.country || 'UK',
      },
    ]);
  });
  it('returns a correct object', () => {
    const identity = 'klaus';
    const author = generateAuthor({
      orcidId: identity,
    });

    const res = setAuthorContributors([author]);

    expect(res.article.front['article-meta']['contrib-group'].contrib).toEqual([
      {
        _attributes: {
          'contrib-type': 'Author',
          corresp: author.isCorresponding ? 'Yes' : 'No',
          submitting: author.isSubmitting ? 'Yes' : 'No',
        },
        'contrib-id': {
          _attributes: { 'contrib-id-type': 'orcid' },
          _text: `https://orcid.org/${identity}`,
        },
        role: { _attributes: { 'content-type': '1' } },
        name: {
          surname: { _text: author.surname },
          'given-names': { _text: author.givenNames },
          prefix: { _text: author.title },
        },
        email: { _text: author.email },
        xref: {
          _attributes: {
            'ref-type': 'aff',
            rid: `I2`,
          },
          sup: {
            _text: 2,
          },
        },
      },
    ]);

    expect(res.article.front['article-meta'].aff).toEqual([
      {
        _attributes: {
          id: `I2`,
        },
        sup: {
          _text: 2,
        },
        'addr-line': { _text: author.aff || '' },
        country: author.country || 'UK',
      },
    ]);
  });
});
