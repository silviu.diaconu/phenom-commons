import { setQuestions } from '../lib/setQuestions';
import { Manuscript } from '@hindawi/phenom-events/src/lib/manuscript';

describe('setQuestions setter', () => {
  it('returns a correct object if questions are undefined', () => {
    const conflictOfInterest = undefined;
    const dataAvailability = undefined;
    const fundingStatement = undefined;

    const res = setQuestions({
      conflictOfInterest,
      dataAvailability,
      fundingStatement,
    } as Manuscript);

    expect(res.article.front.questions.question).toEqual([
      {
        _attributes: { type: 'COI' },
        answer: { _text: 'No' },
        statement: {
          _text:
            'The authors for this paper did not provide a conflict of interest statement',
        },
      },
      {
        _attributes: { type: 'DA' },
        answer: { _text: 'No' },
        statement: {
          _text:
            'The authors for this paper did not provide a data availability statement',
        },
      },
      {
        _attributes: { type: 'Fund' },
        answer: { _text: 'No' },
        statement: {
          _text:
            'The authors for this paper did not provide a funding statement',
        },
      },
    ]);
  });
  it('returns a correct object', () => {
    const conflictOfInterest = 'third-coi';
    const dataAvailability = 'some-availability';
    const fundingStatement = 'some-funding-statement';

    const res = setQuestions({
      conflictOfInterest,
      dataAvailability,
      fundingStatement,
    } as Manuscript);

    expect(res.article.front.questions.question).toEqual([
      {
        _attributes: { type: 'COI' },
        answer: { _text: 'Yes' },
        statement: {
          _text: conflictOfInterest,
        },
      },
      {
        _attributes: { type: 'DA' },
        answer: { _text: 'Yes' },
        statement: {
          _text: dataAvailability,
        },
      },
      {
        _attributes: { type: 'Fund' },
        answer: { _text: 'Yes' },
        statement: {
          _text: fundingStatement,
        },
      },
    ]);
  });
});
