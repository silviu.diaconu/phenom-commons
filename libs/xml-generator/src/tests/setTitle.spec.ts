import { setTitle } from '../lib/setManuscriptData/setTitle';

describe('setTitle setter', () => {
  it('returns a correct object', () => {
    const title = 'some-title'

    const res = setTitle({ title })

    expect(res.article.front['article-meta']['title-group']).toEqual({
      'article-title': {
        _text: title,
      },
    })
  })
})
