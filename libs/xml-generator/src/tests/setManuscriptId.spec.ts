import { setManuscriptId } from '../lib/setManuscriptData/setManuscriptId';

describe('setManuscriptId setter', () => {
  it('throws an error if the journal is not loaded', () => {
    try {
      setManuscriptId({})
    } catch (err) {
      expect(err.message).toEqual('The journal is not loaded')
    }
  })
  it('throws an error if the journal preprints are not loaded', () => {
    try {
      setManuscriptId({ journal: {} })
    } catch (err) {
      expect(err.message).toEqual('The journal preprints are not loaded')
    }
  })
  it('returns a correct object if the journal preprints are empty', () => {
    const manuscript = {
      customId: '12345',
      journal: { code: 'BCA', journalPreprints: [] },
    }

    const parsedManuscriptId = `${manuscript.journal.code}-${manuscript.customId}`

    const res = setManuscriptId(manuscript)

    expect(res.article.front['article-meta']['article-id']).toEqual([
      {
        _attributes: { 'pub-id-type': 'publisher-id' },
        _text: parsedManuscriptId,
      },
      {
        _attributes: { 'pub-id-type': 'manuscript' },
        _text: parsedManuscriptId,
      },
    ])
  })
  it('returns a correct object', () => {
    const manuscript = {
      customId: '12345',
      preprintValue: 'arXiv-p3n15',
      journal: {
        code: 'BCA',
        journalPreprints: [
          {
            preprint: {
              type: 'arXiv',
            },
          },
        ],
      },
    }

    const parsedManuscriptId = `${manuscript.journal.code}-${manuscript.customId}`

    const res = setManuscriptId(manuscript)

    expect(res.article.front['article-meta']['article-id']).toEqual([
      {
        _attributes: { 'pub-id-type': 'publisher-id' },
        _text: parsedManuscriptId,
      },
      {
        _attributes: { 'pub-id-type': 'manuscript' },
        _text: parsedManuscriptId,
      },
      {
        _attributes: {
          'pub-id-type': manuscript.journal.journalPreprints[0].preprint.type,
        },
        _text: manuscript.preprintValue,
      },
    ])
  })
})
