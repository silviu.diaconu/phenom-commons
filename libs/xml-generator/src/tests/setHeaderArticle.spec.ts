import { setHeaderArticle } from '../lib/setHeaderArticle';

describe('setHeader setter', () => {
  it('returns a correct object', () => {
    const articleType = 'some article type'
    const res = setHeaderArticle(articleType)

    expect(res.article).toEqual({
      _attributes: {
        'dtd-version': '1.1d1',
        'article-type': articleType,
      },
    })
  })
})
