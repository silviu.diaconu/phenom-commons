import { setSection } from '../lib/setManuscriptData/setSection';


describe('setSection setter', () => {
  it('returns an empty object if no section name is provided', () => {
    const sectionName = undefined

    const res = setSection({ sectionName })

    expect(res).toEqual({})
  })
  it('returns a correct object', () => {
    const sectionName = 'some-name'

    const res = setSection({ sectionName })

    expect(res.article.front['article-meta']['subject-area']).toEqual({
      _text: sectionName,
    })
  })
})
