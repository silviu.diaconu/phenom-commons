import { setDate } from '../lib/setManuscriptData/setDate';

describe('setDate setter', () => {
  it('returns a correct object', () => {
    const date = new Date()
    const dateType = 'submitted'

    const res = setDate({ date, dateType })

    expect(res.article.front['article-meta'].history.date).toEqual([
      {
        _attributes: { 'date-type': dateType },
        day: { _text: date.getDate() },
        month: { _text: date.getMonth() + 1 },
        year: { _text: date.getFullYear() },
      },
    ])
  })
})
