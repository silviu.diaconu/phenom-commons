import { setArticleType } from '../lib/setManuscriptData/setArticleType';


describe('setArticleType setter', () => {
  it('returns a correct object', () => {
    const articleTypeName = 'article-name'

    const res = setArticleType({ articleTypeName })

    expect(res.article.front['article-meta']['article-categories']).toEqual({
      'subj-group': [
        {
          _attributes: {
            'subj-group-type': 'heading',
          },
          subject: {
            _text: articleTypeName,
          },
        },
      ],
    })
  })
})
