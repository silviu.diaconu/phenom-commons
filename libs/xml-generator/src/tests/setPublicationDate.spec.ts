import { setPublicationDate } from '../lib/setManuscriptData/setPublicationDate';

describe('setPublicationDate setter', () => {
  it('always returns an object with the current year', () => {
    const date = new Date()

    const res = setPublicationDate()

    expect(res.article.front['article-meta']['pub-date']).toEqual({
      _attributes: {
        'pub-type': 'publication-year',
      },
      year: {
        _text: date.getFullYear(),
      },
    })
  })
})
