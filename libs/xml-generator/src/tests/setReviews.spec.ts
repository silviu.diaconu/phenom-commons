import { setReviews } from '../lib/setReviews';
// test-utils is used only for testing, it's ok to import here.
// eslint-disable-next-line @nrwl/nx/enforce-module-boundaries
import { generateComment, generateFile, generateReview, generateAuthor } from '@phenom-commons/test-utils';


describe('setReviews setter', () => {
  it('throws an error if the review does not have a loaded reviewer', () => {
    expect(true)
    // const review = generateReview();
    // const reviews = [review];
    //
    // try {
    //   setReviews({ revType: undefined, affStartingPosition: undefined, reviews });
    // } catch (err) {
    //   expect(err.message).toEqual(
    //     `Review ${review.id} does not have a loaded reviewer`
    //   );
    // }
  });
  // it('throws an error if the review does no have loaded comments', () => {
  //   const member = generateAuthor();
  //   const review = generateReview({ member });
  //   const reviews = [review];
  //
  //   try {
  //     setReviews({ revType: undefined, affStartingPosition: undefined, reviews });
  //   } catch (err) {
  //     expect(err.message).toEqual(
  //       `Review ${review.id} does not have loaded comments`
  //     );
  //   }
  // });
  // it('throws an error if the review does no have loaded comment files', () => {
  //   const member = generateAuthor();
  //   const comment = generateComment({ type: 'public' });
  //   const comments = [comment];
  //   const review = generateReview({ member, comments });
  //   const reviews = [review];
  //
  //   try {
  //     setReviews({ revType: undefined, affStartingPosition: undefined, reviews });
  //   } catch (err) {
  //     expect(err.message).toEqual(
  //       `Review ${review.id} does not have loaded comment files`
  //     );
  //   }
  // });
  // it('returns an empty object if no reviews are provided', () => {
  //   const reviews = [];
  //
  //   const res = setReviews({ revType: undefined, affStartingPosition: undefined, reviews });
  //
  //   expect(res).toEqual({});
  // });
  // it('returns a correct object', () => {
  //   const file = generateFile({ type: 'reviewComment' });
  //   const member = generateAuthor({ responded: new Date() });
  //   const comment = generateComment({ type: 'public', files: [file] });
  //   const review = generateReview({
  //     member,
  //     comments: [comment],
  //     submitted: new Date()
  //   });
  //   const revType = 'reviewComment';
  //   const affStartingPosition = 0;
  //
  //   const res = setReviews({ reviews: [review], revType, affStartingPosition });
  //
  //   const commentTemplate = [
  //     {
  //       _attributes: { 'comment-type': 'comment' },
  //       _text: comment.content || ''
  //     }
  //   ];
  //
  //   const filesTemplate = [
  //     {
  //       item_type: { _text: 'review' },
  //       item_description: { _text: file.originalName },
  //       item_name: { _text: file.fileName }
  //     }
  //   ];
  //
  //   const reviewTemplate = {
  //     _attributes: { 'rev-type': revType },
  //     name: {
  //       surname: { _text: member.surname },
  //       'given-names': { _text: member.givenNames },
  //       prefix: { _text: member.title || 'Dr.' }
  //     },
  //     email: { _text: member.email },
  //     xref: {
  //       _attributes: {
  //         'ref-type': 'aff',
  //         rid: 'I1'
  //       },
  //       sup: { _text: 1 }
  //     },
  //     date: [
  //       {
  //         _attributes: { 'date-type': 'assignment' },
  //         day: { _text: member.responded.getDate() },
  //         month: { _text: member.responded.getMonth() + 1 },
  //         year: { _text: member.responded.getFullYear() }
  //       },
  //       {
  //         _attributes: { 'date-type': 'submission' },
  //         day: { _text: review.submitted.getDate() },
  //         month: { _text: review.submitted.getMonth() + 1 },
  //         year: { _text: review.submitted.getFullYear() }
  //       }
  //     ],
  //     comment: commentTemplate,
  //     files: { file: filesTemplate },
  //     recommendation: review.recommendation || ''
  //   };
  //
  //   expect(res.article.front['rev-group'].rev).toEqual([reviewTemplate]);
  // });
  // it('returns a correct object if comment has no files', () => {
  //   const member = generateAuthor({ responded: new Date() });
  //   const comment = generateComment({ type: 'public', files: [] });
  //   const review = generateReview({
  //     member,
  //     comments: [comment],
  //     submitted: new Date()
  //   });
  //   const revType = 'reviewComment';
  //   const affStartingPosition = 0;
  //
  //   const res = setReviews({ reviews: [review], revType, affStartingPosition });
  //
  //   const commentTemplate = [
  //     {
  //       _attributes: { 'comment-type': 'comment' },
  //       _text: comment.content || ''
  //     }
  //   ];
  //
  //   const reviewTemplate = {
  //     _attributes: { 'rev-type': revType },
  //     name: {
  //       surname: { _text: member.surname },
  //       'given-names': { _text: member.givenNames },
  //       prefix: { _text: member.title || 'Dr.' }
  //     },
  //     email: { _text: member.email },
  //     xref: {
  //       _attributes: {
  //         'ref-type': 'aff',
  //         rid: 'I1'
  //       },
  //       sup: { _text: 1 }
  //     },
  //     date: [
  //       {
  //         _attributes: { 'date-type': 'assignment' },
  //         day: { _text: member.responded.getDate() },
  //         month: { _text: member.responded.getMonth() + 1 },
  //         year: { _text: member.responded.getFullYear() }
  //       },
  //       {
  //         _attributes: { 'date-type': 'submission' },
  //         day: { _text: review.submitted.getDate() },
  //         month: { _text: review.submitted.getMonth() + 1 },
  //         year: { _text: review.submitted.getFullYear() }
  //       }
  //     ],
  //     comment: commentTemplate,
  //     files: { file: [] },
  //     recommendation: review.recommendation || ''
  //   };
  //
  //   expect(res.article.front['rev-group'].rev).toEqual([reviewTemplate]);
  // });
  // it('returns a correct object if review has no comments', () => {
  //   const member = generateAuthor({ responded: new Date() });
  //   const review = generateReview({
  //     member,
  //     comments: [],
  //     submitted: new Date()
  //   });
  //   const revType = 'reviewComment';
  //   const affStartingPosition = 0;
  //
  //   const res = setReviews({ reviews: [review], revType, affStartingPosition });
  //
  //   const reviewTemplate = {
  //     _attributes: { 'rev-type': revType },
  //     name: {
  //       surname: { _text: member.surname },
  //       'given-names': { _text: member.givenNames },
  //       prefix: { _text: member.title || 'Dr.' }
  //     },
  //     email: { _text: member.email },
  //     xref: {
  //       _attributes: {
  //         'ref-type': 'aff',
  //         rid: 'I1'
  //       },
  //       sup: { _text: 1 }
  //     },
  //     date: [
  //       {
  //         _attributes: { 'date-type': 'assignment' },
  //         day: { _text: member.responded.getDate() },
  //         month: { _text: member.responded.getMonth() + 1 },
  //         year: { _text: member.responded.getFullYear() }
  //       },
  //       {
  //         _attributes: { 'date-type': 'submission' },
  //         day: { _text: review.submitted.getDate() },
  //         month: { _text: review.submitted.getMonth() + 1 },
  //         year: { _text: review.submitted.getFullYear() }
  //       }
  //     ],
  //     comment: [],
  //     files: { file: [] },
  //     recommendation: review.recommendation || ''
  //   };
  //
  //   expect(res.article.front['rev-group'].rev).toEqual([reviewTemplate]);
  // });
});
