import { setCounts } from '../lib/setManuscriptData/setCounts';

describe('setCounts setter', () => {
  it('returns an empty object if figureFilesCount is undefined', () => {
    const figureFilesCount = undefined

    const res = setCounts({ figureFilesCount })

    expect(res).toEqual({})
  })
  it('returns a correct object', () => {
    const figureFilesCount = '456'

    const res = setCounts({ figureFilesCount })

    expect(res.article.front['article-meta'].counts).toEqual({
      'fig-count': {
        _attributes: {
          count: figureFilesCount,
        },
      },
      'ref-count': {
        _attributes: {
          count: '000',
        },
      },
      'page-count': {
        _attributes: {
          count: '000',
        },
      },
    })
  })
})
