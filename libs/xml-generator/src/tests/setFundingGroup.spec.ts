import { setFundingGroup } from '../lib/setManuscriptData/setFundingGroup';

describe('setFundingGroup setter', () => {
  it('always returns an empty array', () => {
    const res = setFundingGroup()
    expect(res.article.front['article-meta']['funding-group']).toEqual([])
  })
})
