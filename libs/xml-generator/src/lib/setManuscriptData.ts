import { get, merge } from 'lodash';
import { setPublicationDate } from './setManuscriptData/setPublicationDate';
import { setManuscriptId } from './setManuscriptData/setManuscriptId';
import { setVersion } from './setManuscriptData/setVersion';
import { setTitle } from './setManuscriptData/setTitle';
import { setAbstract } from './setManuscriptData/setAbstract';
import { setArticleType } from './setManuscriptData/setArticleType';
import { setSection } from './setManuscriptData/setSection';
import { setSpecialIssue } from './setManuscriptData/setSpecialIssue';
import { setHistory } from './setManuscriptData/setHistory';
import { setContributors } from './setManuscriptData/setContributors';
import { setFundingGroup } from './setManuscriptData/setFundingGroup';
import { setCounts } from './setManuscriptData/setCounts';
import { Author } from '@hindawi/phenom-events/src/lib/author';
import { Manuscript } from '@hindawi/phenom-events/src/lib/manuscript';

export const setManuscriptData = ({
                                    authors,
                                    customizer,
                                    manuscript,
                                    figureFiles,
                                    academicEditor,
                                  }: {
  authors:Author[],
  customizer,
  manuscript:Manuscript,
  figureFiles,
  academicEditor,
}) => {
  if (!manuscript) throw new Error('Manuscript was not provided');

  const publicationDateTemplate = setPublicationDate();
  const manuscriptIdTemplate = setManuscriptId(manuscript);
  const versionTemplate = setVersion({ version: manuscript.version });
  const titleTemplate = setTitle({ title: manuscript.title });
  const abstractTemplate = setAbstract({ abstract: manuscript.abstract });
  const articleTypeTemplate = setArticleType({
    articleTypeName: manuscript.articleType.name,
  });
  const sectionTemplate = setSection({
    sectionName: get(manuscript, 'section.name'),
  });
  const specialIssueTemplate = setSpecialIssue({
    specialIssueId: get(manuscript, 'specialIssue.customId'),
    specialIssueName: get(manuscript, 'specialIssue.name'),
  });
  const historyTemplate = setHistory({
    customizer,
    submittedDate: new Date(),
    finalRevisionDate: new Date(),
    peerReviewPassedDate: new Date(),
    //todo
    // submittedDate: manuscript.submittedDate,
    // finalRevisionDate: manuscript.updated, // wrong
    // peerReviewPassedDate: manuscript.peerReviewPassedDate,
  });
  const contributorsTemplate = setContributors({
    authors,
    customizer,
    academicEditor,
  });
  const fundingGroupTemplate = setFundingGroup();
  const countsTemplate = setCounts({
    figureFilesCount: figureFiles && figureFiles.length,
  });

  return merge(
    publicationDateTemplate,
    manuscriptIdTemplate,
    articleTypeTemplate,
    versionTemplate,
    titleTemplate,
    contributorsTemplate,
    historyTemplate,
    abstractTemplate,
    fundingGroupTemplate,
    sectionTemplate,
    specialIssueTemplate,
    countsTemplate,
  );
};

module.exports = {
  setManuscriptData,
};
