import { set } from 'lodash';
import { PhenomFile } from '@hindawi/phenom-events/src/lib/phenomFile';

export const setFiles = (files: PhenomFile[]) => {
  if (!files.length) return {};
  const templateFiles = files.map(file => ({
    item_type: {
      _text: file.type,
    },
    item_description: {
      _text: file.originalName,
    },
    item_name: {
      _text: file.fileName,
    },
  }));

  return set({}, 'article.front.files.file', templateFiles);
};

module.exports = {
  setFiles,
};
