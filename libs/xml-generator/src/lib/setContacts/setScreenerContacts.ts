import { set } from 'lodash';


const screenerRolesMap = {
  es: 'ES',
  qc: 'QC',
  eqa: 'EQA',
  esLeader: 'ESLeader',
  qcLeader: 'QCLeader',
  mcLeader: 'MCLeader',
}

export const setScreenerContacts = ({ screeners }) => {
  if (!screeners || !screeners.length) return {}

  const screenersTemplate = screeners.map(screener => ({
    _attributes: { role: screenerRolesMap[screener.role] },
    name: {
      surname: { _text: screener.surname },
      'given-names': { _text: screener.givenNames },
    },
    email: { _text: screener.email },
  }))

  return set({}, 'article.front.contacts.contact-person', screenersTemplate)
}

module.exports = {
  setScreenerContacts,
}
