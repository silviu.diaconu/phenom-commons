import { set } from 'lodash';
import { Editor } from '@hindawi/phenom-events/src/lib/editor';


export const setEditorialAssistantContact = ({ editorialAssistant }: { editorialAssistant: Editor }) => {
  if (!editorialAssistant) return {};

  const editorialAssistantTemplate = {
    _attributes: { role: 'EA' },
    name: {
      surname: { _text: editorialAssistant.surname },
      'given-names': { _text: editorialAssistant.givenNames },
    },
    email: { _text: editorialAssistant.email },
  };

  return set({}, 'article.front.contacts.contact-person', [
    editorialAssistantTemplate,
  ]);
};

module.exports = {
  setEditorialAssistantContact,
};
