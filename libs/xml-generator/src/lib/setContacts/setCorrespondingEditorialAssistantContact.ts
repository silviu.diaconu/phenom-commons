import { set } from 'lodash';
import { Editor } from '@hindawi/phenom-events/src/lib/editor';


export const setCorrespondingEditorialAssistantContact = ({ correspondingEditorialAssistant }: { correspondingEditorialAssistant: Editor }) => {
  if (!correspondingEditorialAssistant) return {};

  const editorialAssistantTemplate = {
    _attributes: { role: 'EALeader' },
    name: {
      surname: { _text: correspondingEditorialAssistant.surname },
      'given-names': {
        _text: correspondingEditorialAssistant.givenNames,
      },
    },
    email: { _text: correspondingEditorialAssistant.email },
  };

  return set({}, 'article.front.contacts.contact-person', [
    editorialAssistantTemplate,
  ]);
};

module.exports = {
  setCorrespondingEditorialAssistantContact,
};
