import { isArray, merge } from 'lodash';
import convert from 'xml-js';
import { setManuscriptData } from './setManuscriptData';
import { setJournalMeta } from './setJournalMeta';
import { setQuestions } from './setQuestions';
import { setContacts } from './setContacts';
import { setRevGroup } from './setRevGroup';
import { setHeader } from './setHeader';
import { setHeaderArticle } from './setHeaderArticle';
import { setFiles } from './setFiles';
import { GenerateXmlInput } from './contract';


const customizer = (objValue, srcValue) => {
  if (isArray(objValue)) return objValue.concat(srcValue);
};

export const generateXML = async (input: GenerateXmlInput) => {
  const {
    authors,
    reviews,
    TeamRoles,
    screeners,
    manuscript,
    figureFiles,
    academicEditor,
    editorialAssistant,
    correspondingEditorialAssistant,
  } = { ...input };
  const xmlHeader = setHeader();
  const headerArticle = setHeaderArticle(manuscript.articleType.name);
  const manuscriptData = setManuscriptData({
    authors,
    manuscript,
    customizer,
    figureFiles,
    academicEditor,
  });
  const files = setFiles(manuscript.files);
  const questions = setQuestions(manuscript);
  //todo
  // const journalMeta = setJournalMeta(manuscript.journal);
  const revGroup = setRevGroup({
    reviews,
    TeamRoles,
    customizer,
    // manuscript,
  });

  const contacts = setContacts({
    screeners,
    customizer,
    editorialAssistant,
    correspondingEditorialAssistant,
  });

  const templateObject = merge(
    xmlHeader,
    headerArticle,
    //todo
    // journalMeta,
    manuscriptData,
    files,
    questions,
    revGroup,
    contacts,
  );

  return convert.json2xml(templateObject, {
    compact: true,
    ignoreComment: true,
    spaces: 2,
    fullTagEmptyElement: true,
  });
};
