import { set } from 'lodash';

export const setCounts = ({ figureFilesCount }: { figureFilesCount: number | string }) => {
  if (figureFilesCount === undefined) return {};

  const countsTemplate = {
    'fig-count': {
      _attributes: {
        count: figureFilesCount.toString().padStart(3, '0'),
      },
    },
    'ref-count': {
      _attributes: {
        count: '000',
      },
    },
    'page-count': {
      _attributes: {
        count: '000',
      },
    },
  };

  return set({}, 'article.front.article-meta.counts', countsTemplate);
};

module.exports = {
  setCounts,
};
