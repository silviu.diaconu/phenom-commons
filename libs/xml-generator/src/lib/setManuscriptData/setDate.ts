import { set } from 'lodash';

export const setDate = ({ date, dateType }: { date, dateType: string }) => {
  const parsedDate = new Date(date);

  const dateTemplate = {
    _attributes: { 'date-type': dateType },
    day: { _text: parsedDate.getDate() },
    month: { _text: parsedDate.getMonth() + 1 },
    year: { _text: parsedDate.getFullYear() },
  };

  return set({}, 'article.front.article-meta.history.date', [dateTemplate]);
};

module.exports = { setDate };
