import { mergeWith } from 'lodash';
import { setAuthorContributors } from './setContributors/setAuthorContributors';
import { setAcademicEditorContributor } from './setContributors/setAcademicEditorContributor';
import { Author } from '@hindawi/phenom-events/src/lib/author';
import { Editor } from '@hindawi/phenom-events/src/lib/editor';


export const setContributors = ({ authors, academicEditor, customizer }: {
  authors: Author[], academicEditor: Editor, customizer
}) => {
  const authorContributorsTemplate = setAuthorContributors(authors);
  const academicEditorContributorTemplate = setAcademicEditorContributor(
    academicEditor,
  );

  return mergeWith(
    authorContributorsTemplate,
    academicEditorContributorTemplate,
    customizer, // we need this to merge the contributors array within objects
  );
};

module.exports = {
  setContributors,
};
