import { set } from 'lodash';

export const setAbstract = ({ abstract }: { abstract: string }) => {
  const abstractTemplate = {
    _text: abstract,
  };

  return set({}, 'article.front.article-meta.abstract', abstractTemplate);
};

module.exports = {
  setAbstract,
};
