import { set } from 'lodash';

export const setArticleType = ({ articleTypeName }: { articleTypeName: string }) => {
  const articleTypeTemplate = {
    'subj-group': [
      {
        _attributes: {
          'subj-group-type': 'heading',
        },
        subject: {
          _text: articleTypeName,
        },
      },
    ],
  };

  return set(
    {},
    'article.front.article-meta.article-categories',
    articleTypeTemplate,
  );
};

module.exports = {
  setArticleType,
};
