import { Editor } from '@hindawi/phenom-events/src/lib/editor';
import { merge, set } from 'lodash';

export const setAcademicEditorContributor = (academicEditor: Editor) => {
  if (!academicEditor) return;

  // if (!academicEditor.user)
  //   throw new Error(`Editor ${academicEditor.id} does not have a loaded user`);
  // if (!academicEditor.user.identities)
  //   throw new Error(
  //     `Editor ${academicEditor.id} does not have loaded identities`,
  //   );

  const orcidIdentity = academicEditor.orcidId;

  const academicEditorTemplate = {
    _attributes: { 'contrib-type': 'Academic Editor' },
    role: { _attributes: { 'content-type': '1' } },
    name: {
      surname: { _text: academicEditor.surname },
      'given-names': { _text: academicEditor.givenNames },
      prefix: { _text: academicEditor.title },
    },
    email: { _text: academicEditor.email },
    xref: {
      _attributes: {
        'ref-type': 'aff',
        rid: `I1`,
      },
      sup: {
        _text: 1,
      },
    },
  };

  if (orcidIdentity) {
    set(academicEditorTemplate, 'contrib-id', {
      _attributes: { 'contrib-id-type': 'orcid' },
      _text: `https://orcid.org/${orcidIdentity}`,
    });
  }

  const setAcademicEditor = set(
    {},
    'article.front.article-meta.contrib-group.contrib',
    [academicEditorTemplate],
  );

  const academicEditorAffiliationTemplate = {
    _attributes: {
      id: `I1`,
    },
    sup: {
      _text: 1,
    },
    'addr-line': { _text: academicEditor.aff || '' },
    country: academicEditor.country || 'UK',
  };

  const setAcademicEditorAffiliation = set(
    {},
    'article.front.article-meta.aff',
    [academicEditorAffiliationTemplate],
  );

  return merge(setAcademicEditor, setAcademicEditorAffiliation);
};

module.exports = {
  setAcademicEditorContributor,
};
