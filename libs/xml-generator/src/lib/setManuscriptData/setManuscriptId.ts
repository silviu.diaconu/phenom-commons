import { set } from 'lodash';

//todo to modify
export const setManuscriptId = manuscript => {
  if (!manuscript.journal) throw new Error('The journal is not loaded')
  if (!manuscript.journal.journalPreprints)
    throw new Error('The journal preprints are not loaded')

  const {
    customId,
    preprintValue: manuscriptPreprintValue,
    journal: { code: journalCode, journalPreprints },
  } = manuscript

  const parsedManuscriptId = `${journalCode}-${customId}`

  const manuscriptIdTemplate = [
    {
      _attributes: {
        'pub-id-type': 'publisher-id',
      },
      _text: parsedManuscriptId,
    },
    {
      _attributes: {
        'pub-id-type': 'manuscript',
      },
      _text: parsedManuscriptId,
    },
  ]

  const preprintIdsTemplate = journalPreprints.map(journalPreprint => ({
    _attributes: {
      'pub-id-type': journalPreprint.preprint.type,
    },
    _text: manuscriptPreprintValue,
  }))

  return set({}, 'article.front.article-meta.article-id', [
    ...manuscriptIdTemplate,
    ...preprintIdsTemplate,
  ])
}

module.exports = {
  setManuscriptId,
}
