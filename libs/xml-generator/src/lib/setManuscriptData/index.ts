import { setAbstract } from './setAbstract';
import { setArticleType } from './setArticleType';
import { setContributors } from './setContributors';
import { setCounts } from './setCounts';
import { setDate } from './setDate';
import { setFundingGroup } from './setFundingGroup';
import { setHistory } from './setHistory';
import { setManuscriptId } from './setManuscriptId';
import { setPublicationDate } from './setPublicationDate';
import { setSection } from './setSection';
import { setSpecialIssue } from './setSpecialIssue';
import { setTitle } from './setTitle';
import { setVersion } from './setVersion';


module.exports = {
  setAbstract,
  setArticleType,
  setContributors,
  setCounts,
  setDate,
  setFundingGroup,
  setHistory,
  setManuscriptId,
  setPublicationDate,
  setSection,
  setSpecialIssue,
  setTitle,
  setVersion
};
