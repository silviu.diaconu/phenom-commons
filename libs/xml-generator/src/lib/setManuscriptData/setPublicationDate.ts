import { set } from 'lodash';

export const setPublicationDate = () => {
  const date = new Date()
  const publicationDateTemplate = {
    _attributes: {
      'pub-type': 'publication-year',
    },
    year: {
      _text: date.getFullYear(),
    },
  }

  return set({}, 'article.front.article-meta.pub-date', publicationDateTemplate)
}

module.exports = {
  setPublicationDate,
}
