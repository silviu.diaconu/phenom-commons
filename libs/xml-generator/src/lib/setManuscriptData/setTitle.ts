import { set } from 'lodash';

export const setTitle = ({ title }: { title: string }) => {
  const titleTemplate = {
    'article-title': {
      _text: title,
    },
  };

  return set({}, 'article.front.article-meta.title-group', titleTemplate);
};

module.exports = {
  setTitle,
};
