import { set } from 'lodash';

export const setSection = ({ sectionName }) => {
  if (!sectionName) return {}

  const sectionTemplate = {
    'subject-area': {
      _text: sectionName,
    },
  }

  return set({}, 'article.front.article-meta', sectionTemplate)
}

module.exports = {
  setSection,
}
