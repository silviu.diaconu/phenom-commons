import { set } from 'lodash';

export const setFundingGroup = () =>
  set({}, 'article.front.article-meta.funding-group', [])

module.exports = {
  setFundingGroup,
}
