import { set } from 'lodash';

export const setSpecialIssue = ({ specialIssueId, specialIssueName }:{ specialIssueId:string, specialIssueName:string }) => {
  if (!specialIssueName || !specialIssueId) return {}

  const specialIssueTemplate = {
    'special-issue-id': {
      _text: specialIssueId,
    },
    'special-issue-title': {
      _text: specialIssueName,
    },
  }

  return set({}, 'article.front.article-meta', specialIssueTemplate)
}

module.exports = {
  setSpecialIssue,
}
