import { merge, set } from 'lodash';
import { Review } from '@hindawi/phenom-events/src/lib/review';
import { Reviewer } from '@hindawi/phenom-events/src/lib/reviewer';


export const setReviews = ({ revType, reviews, reviewers, affStartingPosition }:
                             {
                               revType,
                               reviews: Review[],
                               reviewers: Reviewer[]
                               affStartingPosition
                             }) => {
  // if (!reviews || !reviews.length) return {};
  //
  // const reviewersTemplate = reviews.map((review, i) => {
  //   const { comments } = review;
  //   const reviewer: Reviewer = reviewers.find(r => r.id === review.reviewerId);
  //
  //   if (!reviewer)
  //     throw new Error(`Review ${review.id} does not have a loaded reviewer`);
  //   if (!comments)
  //     throw new Error(`Review ${review.id} does not have loaded comments`);
  //
  //   const affId = `I${affStartingPosition + i + 1}`;
  //   const assignmentDate = new Date(reviewer.responded || reviewer.created);
  //   const submissionDate = new Date(review.submitted);
  //
  //   const reviewCommentsTemplate = comments.map(comment => ({
  //     _attributes: {
  //       'comment-type': comment.type === 'public' ? 'comment' : 'confidential',
  //     },
  //     _text: comment.content || '',
  //   }));
  //
  //   const publicComment = comments.find(comment => comment.type === 'public');
  //
  //   let reviewFilesTemplate = [];
  //   if (publicComment) {
  //     const { files: publicFiles } = publicComment;
  //
  //     if (!publicFiles)
  //       throw new Error(
  //         `Review ${review.id} does not have loaded comment files`,
  //       );
  //
  //     reviewFilesTemplate = publicFiles.map(file => ({
  //       item_type: {
  //         _text: file.type === "" ? 'review' : file.type,
  //       },
  //       item_description: { _text: file.originalName },
  //       item_name: { _text: file.fileName },
  //     }));
  //   }
  //
  //   const reviewTemplate = {
  //     _attributes: {
  //       'rev-type': revType,
  //     },
  //     name: {
  //       surname: {
  //         _text: reviewer.alias.surname,
  //       },
  //       'given-names': {
  //         _text: reviewer.alias.givenNames,
  //       },
  //       prefix: {
  //         _text: reviewer.alias.title || 'Dr.',
  //       },
  //     },
  //     email: {
  //       _text: reviewer.alias.email,
  //     },
  //     xref: {
  //       _attributes: {
  //         'ref-type': 'aff',
  //         rid: affId,
  //       },
  //       sup: {
  //         _text: i + 1,
  //       },
  //     },
  //     date: [
  //       {
  //         _attributes: {
  //           'date-type': 'assignment',
  //         },
  //         day: {
  //           _text: assignmentDate.getDate(),
  //         },
  //         month: {
  //           _text: assignmentDate.getMonth() + 1,
  //         },
  //         year: {
  //           _text: assignmentDate.getFullYear(),
  //         },
  //       },
  //       {
  //         _attributes: {
  //           'date-type': 'submission',
  //         },
  //         day: {
  //           _text: submissionDate.getDate(),
  //         },
  //         month: {
  //           _text: submissionDate.getMonth() + 1,
  //         },
  //         year: {
  //           _text: submissionDate.getFullYear(),
  //         },
  //       },
  //     ],
  //     comment: reviewCommentsTemplate,
  //     files: { file: reviewFilesTemplate },
  //     recommendation: review.recommendation || '',
  //   };
  //
  //   const reviewAffiliationTemplate = {
  //     _attributes: { id: affId },
  //     country: reviewer.alias.country || 'UK',
  //     'addr-line': { _text: reviewer.alias.aff || '' },
  //   };
  //
  //   return { reviewTemplate, reviewAffiliationTemplate };
  // });
  //
  // const setReviews = set(
  //   {},
  //   'article.front.rev-group.rev',
  //   reviewersTemplate.map(template => template.reviewTemplate),
  // );
  // const setReviewAffiliations = set(
  //   {},
  //   'article.front.rev-group.aff',
  //   reviewersTemplate.map(template => template.reviewAffiliationTemplate),
  // );
  //
  // return merge(setReviews, setReviewAffiliations);
};

module.exports = {
  setReviews,
};
