import { Author } from '@hindawi/phenom-events/src/lib/author';
import { Manuscript } from '@hindawi/phenom-events/src/lib/manuscript';
import { Review } from '@hindawi/phenom-events/src/lib/review';

export interface GenerateXmlInput {
  authors: Author[],
  reviews?: Review[],
  TeamRoles?,
  screeners?,
  manuscript: Manuscript,
  figureFiles?,
  academicEditor?,
  editorialAssistant,
  correspondingEditorialAssistant,
}
