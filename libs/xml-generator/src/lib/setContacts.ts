import { mergeWith } from 'lodash';
import { setEditorialAssistantContact } from './setContacts/setEditorialAssistantContact';
import { setCorrespondingEditorialAssistantContact } from './setContacts/setCorrespondingEditorialAssistantContact';
import { setScreenerContacts } from './setContacts/setScreenerContacts';


export const setContacts = ({
                       screeners,
                       customizer,
                       editorialAssistant,
                       correspondingEditorialAssistant
                     }) => {
  const correspondingEditorialAssistantTemplate = setCorrespondingEditorialAssistantContact(
    { correspondingEditorialAssistant }
  );

  const editorialAssistantTemplate = setEditorialAssistantContact({
    editorialAssistant
  });

  const screenersTemplate = setScreenerContacts({ screeners });

  return mergeWith(
    correspondingEditorialAssistantTemplate,
    editorialAssistantTemplate,
    screenersTemplate,
    customizer // we need this to merge the contacts array within objects
  );
};

module.exports = {
  setContacts
};
