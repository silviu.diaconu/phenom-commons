import { set } from 'lodash';

export const setHeader = () => {
  const headerTemplate = {
    _declaration: {
      _attributes: {
        version: '1.0',
        encoding: 'utf-8',
      },
    },
    _doctype: 'article SYSTEM "JATS-archivearticle1-mathml3.dtd"',
  }

  return set({}, '', headerTemplate)
}

module.exports = {
  setHeader,
}
