import { set } from 'lodash';
import { Manuscript } from '@hindawi/phenom-events/src/lib/manuscript';

export const setQuestions = (manuscript: Manuscript) => {
  const { conflictOfInterest, dataAvailability, fundingStatement } = manuscript;

  const templateQuestions = [
    {
      _attributes: {
        type: 'COI',
      },
      answer: {
        _text: conflictOfInterest ? 'Yes' : 'No',
      },
      statement: {
        _text:
          conflictOfInterest ||
          'The authors for this paper did not provide a conflict of interest statement',
      },
    },
    {
      _attributes: {
        type: 'DA',
      },
      answer: {
        _text: dataAvailability ? 'Yes' : 'No',
      },
      statement: {
        _text:
          dataAvailability ||
          'The authors for this paper did not provide a data availability statement',
      },
    },
    {
      _attributes: {
        type: 'Fund',
      },
      answer: {
        _text: fundingStatement ? 'Yes' : 'No',
      },
      statement: {
        _text:
          fundingStatement ||
          'The authors for this paper did not provide a funding statement',
      },
    },
  ];

  return set({}, 'article.front.questions.question', templateQuestions);
};

module.exports = {
  setQuestions,
};
