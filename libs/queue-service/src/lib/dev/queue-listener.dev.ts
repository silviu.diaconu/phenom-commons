import { testOptions, testOptionsWorker } from '../test/options';
import { QueueServiceFactory } from '../queue-service.factory';


const runIt = async () => {
  const listener = await QueueServiceFactory.getListener(testOptionsWorker, []);
  listener.start();

  listener.handle('syndication', (payload) => {
    console.log(payload);
  });

};

runIt().then(r => 'yas');

