import { createQueueService } from '../legacy.service';
import { testOptions } from '../test/options';

const runIt = async () => {
  const queueService = await createQueueService(testOptions);

  queueService.registerEventHandler({
    event: 'ArticlePublished',
    handler: (data) => {
      console.log(data);
    }
  });

  await queueService.start();
};

runIt().then(r => 'yas');

