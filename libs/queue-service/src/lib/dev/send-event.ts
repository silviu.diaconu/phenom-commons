import { testOptions } from '../test/options';
import { QueueServiceFactory } from '../queue-service.factory';


const send = async () => {

  const publisherService = await QueueServiceFactory.getTopicPublisher<Record<string, unknown>>(testOptions);

  await publisherService.send('ArticlePublished', {
    'id': '1427871',
    'customId': '1427871',
    'articleType': 'Editorial',
    'created': '2020-02-01 00:00:00',
    'updated': '2020-02-01 00:00:00',
    'published': '2020-06-04 12:00:00',
    'doi': '10.1155/2020/1762428',
    'journalId': 'test-1f0d-439b-9d67-b35c0a22bc01',
    'specialIssueId': null,
    'title': 'Advanced Visual Analyses for Smart and Autonomous Vehicles',
    'authors': [
      {
        'isCorresponding': true,
        'aff': 'Turkish General Staff',
        'email': 'zjfang@sues.edu.cn',
        'country': 'Turkey',
        'surname': 'Fang',
        'givenNames': 'Zhijun'
      },
      {
        'isCorresponding': false,
        'aff': 'Department of Industrial Engineering',
        'email': 'hwang@uw.edu',
        'country': 'Turkey',
        'surname': 'Hwang',
        'givenNames': 'Jenq-Neng'
      },
      {
        'isCorresponding': false,
        'aff': 'Department of Anaesthesia',
        'email': 'schuang@ntut.edu.tw',
        'country': 'Australia',
        'surname': 'Huang',
        'givenNames': 'Shih-Chia'
      }
    ],
    'abstract': '',
    'refCount': 0,
    'volume': 2019,
    'hasSupplementaryMaterials': false,
    'figCount': 0,
    'pageCount': 2
  });
};

send().then(r => console.log('sent'));
