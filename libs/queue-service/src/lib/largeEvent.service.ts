import * as AWS from 'aws-sdk';

import { v4 as uuid } from 'uuid';
import { ILargeEventService, LargeEventsOptions } from './queue-service.contract';


export class LargeEventService implements ILargeEventService {
  private storage: AWS.S3;

  constructor(
    private config: LargeEventsOptions
  ) {

    this.storage = new AWS.S3({
      accessKeyId: this.config.accessKeyId,
      secretAccessKey: this.config.secretAccessKey,
      region: this.config.region
    });
  }

  async put(Body: string) {
    const Key = `${this.config.largeEventsPrefix}/${uuid()}`;
    return this.storage
      .putObject({ Body, Bucket: this.config.largeEventsBucket, Key })
      .promise()
      .then(() => `${this.config.largeEventsBucket}::${Key}`);
  }

  async get(url: string) {
    const [Bucket, Key] = url.split('::');

    return this.storage
      .getObject({ Bucket, Key })
      .promise()
      .then(result => result.Body.toString());
  }
}
