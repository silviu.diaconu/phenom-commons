import { uuid } from 'uuidv4';


export class MessageCodec<MT> {


  constructor(private readonly prefixes: string[] = []) {
  }

  decode(m: string): { event: keyof MT; data: Record<string, unknown> } {
    const parsedMessage = JSON.parse(m);
    let hasPrefix = false;
    let message = { ...parsedMessage };
    // if raw message delivery is disabled we need to dig deeper
    if (parsedMessage.Message) {
      message = JSON.parse(parsedMessage.Message);
    }
    if (this.prefixes?.length) {
      this.prefixes.forEach(pref => {
        if (message.event.startsWith(pref)) {
          message.extractedPublisher = this.extractPublisherName(message.event);
          message.event = message.event.slice(pref.length);
          hasPrefix = true;
        }
      });
      if (!hasPrefix) {
        throw new Error(
          `Message type(${message.event}) doesn't start with any of the prefixes(${this.prefixes})`
        );
      }
    }

    return message;
  }

  encode<T extends keyof MT>(
    event: T,
    data: MT[T] | string,
    id: string = uuid(),
    timestamp: string = new Date().toISOString()
  ): string {
    return JSON.stringify({
      id,
      data,
      event: (this.prefixes[0] || '') + event,
      timestamp
    });
  }

  extractPublisherName(eventPrefix: string) {
    const regex = /pen:(.*?):/;
    const output = regex.exec(eventPrefix.toLowerCase());
    if (output) return output[1];
    return null;
  }
}
