export class Utils {


  public static async getQueueUrl(sqs, queueName: string) {
    try {
      const foo = await sqs
        .getQueueUrl({
          QueueName: queueName
        })
        .promise();
      const { QueueUrl } = foo;

      return QueueUrl;

    } catch (error) {
      console.warn(error, 'unable to get the url of the queue(%s)', queueName);
      throw error;
    }
  }
}
