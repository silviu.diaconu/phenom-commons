import { MessageCodec } from './message.codec';
import { SNS, SQS } from 'aws-sdk';
import { LargeEventService } from './largeEvent.service';
import { ILargeEventService, IPublisher } from '@hindawi/queue-service';
import { MessageAttributeMap } from 'aws-sdk/clients/sns';
import { Utils } from './utils';


export class Publisher<MT> implements IPublisher<MT> {
  private PAYLOAD_LIMIT = 262144; // 1kb = 1024bytes => 256 * 1024

  constructor(
    private readonly snqs: SNS | SQS,
    private readonly codec: MessageCodec<MT>,
    private readonly largeEventService: ILargeEventService,
    private readonly publishTo: { topicArn: string } | { queueName: string }
  ) {
  }

  async send<K extends keyof MT>(event: K, data: MT[K] | string, id?, timestamp?, MessageAttributes?: MessageAttributeMap): Promise<void> {

    const message = this.codec.encode(event, data);
    const isLarge = Buffer.byteLength(message) > this.PAYLOAD_LIMIT;

    // eslint-disable-next-line no-useless-catch
    try {
      if (isLarge) {
        // logger.info(`Event larger than 256k. Uploading to S3 and sending a reference.`);
        MessageAttributes = {
          ...MessageAttributes,
          PhenomMessageTooLarge: { DataType: 'String', StringValue: 'Y' }
        };
        const s3Key = await this.largeEventService.put(message);

        const Message = this.codec.encode(event, s3Key, id, timestamp);

        // logger.info(`Publishing message(${event})`);
        await this.dispatch(Message, MessageAttributes);
      } else {
        // logger.info(`Publishing message(${event})`);
        await this.dispatch(message, MessageAttributes);
      }
    } catch (error) {
      // logger.warn(error, 'error publishing message(%s)', event);
      throw error;
    }

    return;
  }

  private async dispatch(message: string, attributes?: MessageAttributeMap) {
    if (this.snqs instanceof SNS) {
      await this.snqs
        .publish({
          Message: message,
          MessageAttributes: attributes,
          TopicArn: this.publishTo['topicArn']
        })
        .promise();
    } else {
      const request = this.snqs
        .sendMessage({
          MessageBody: message,
          MessageAttributes: attributes,
          QueueUrl: await Utils.getQueueUrl(this.snqs, this.publishTo['queueName'])
        });

      request.on('error', err => {
        console.error(err);
      });
      await request.promise();
    }
  }

  //
  // private getEventName(actionName) {
  //   return `${eventNamespace}:${publisherName}:${serviceName}:${actionName}`;
  // }


}
