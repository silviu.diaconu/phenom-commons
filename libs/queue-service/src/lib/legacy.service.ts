import { CreateQueueServiceResult, QueueServiceOptions } from './queue-service.contract';
import { QueueServiceFactory } from './queue-service.factory';


export async function createQueueService(options: QueueServiceOptions): Promise<CreateQueueServiceResult> {

  const listenerService = QueueServiceFactory.getListener<Record<string, unknown>>(options);
  const publisherService = await QueueServiceFactory.getTopicPublisher<Record<string, unknown>>(options);

  async function stop() {
    await listenerService.stop();
  }

  function registerEventHandler({ event, handler }) {
    listenerService.handle(event, handler);
  }

  async function start() {
    await listenerService.start();
  }

  function publishMessage({
                            event,
                            data,
                            messageAttributes,
                            timestamp,
                            id
                          }) {

    publisherService.send(event, data, id, timestamp, messageAttributes);
  }

  return {
    publishMessage, start, registerEventHandler, stop
  };
}






