import { Consumer } from 'sqs-consumer';
import { Message, MessageBodyAttributeMap } from 'aws-sdk/clients/sqs';
import { MessageCodec } from './message.codec';
import {
  ILargeEventService,
  IListener,
  QueueServiceOptions,
  SqsOptions,
} from './queue-service.contract';
import { SQS } from 'aws-sdk';
import { Utils } from './utils';

export type ListenerHandler<TBody, TAttributes = MessageBodyAttributeMap> = (
  message: TBody,
  attributes?: TAttributes
) => Promise<void>;

export class Listener<T> implements IListener<T> {
  private consumer: Consumer;
  private listeners: {
    [key: string]: Array<ListenerHandler<unknown>>;
  } = {};

  constructor(
    private readonly sqs: SQS,
    private readonly largeEventService: ILargeEventService,
    private codec: MessageCodec<T>,
    private readonly options: SqsOptions
  ) {}

  async start(queueName?: string): Promise<void> {
    queueName = queueName || this.options.queueName;
    const queueUrl = await Utils.getQueueUrl(this.sqs, queueName);

    this.consumer = Consumer.create({
      sqs: this.sqs,
      queueUrl,
      handleMessage: this.handleMessage,
      messageAttributeNames: ['type', 'PhenomMessageTooLarge'],
    });

    this.consumer.on('error', this.handleError);
    this.consumer.on('processing_error', this.handleProcessingError);
    this.consumer.start();
    console.log('start SQS consumer');
  }

  stop(): void {
    this.consumer.stop();
    // this.logger.info('stop SQS consumer');
  }

  handle(event, fn): void {
    if (!Array.isArray(event)) {
      event = [event];
    }

    event.forEach((e) => this.handleOne(e, fn));
  }

  handleMap(eventsMap): void {
    for (const ev in eventsMap) {
      this.handle(ev, eventsMap[ev]);
    }
  }

  private handleOne(event, fn): void {
    if (!this.listeners[event as string]) {
      this.listeners[event as string] = [];
    }

    this.listeners[event as string].push(fn);
  }

  private handleMessage = async (msg: Message): Promise<void> => {
    const body = JSON.parse(msg.Body);
    let messageBody;
    let data;

    try {
      messageBody = this.codec.decode(msg.Body as string);
    } catch (error) {
      // this.logger.warn(error, 'Error decoding message (MessageId=%s)', msg.MessageId);
      return;
    }
    messageBody.phenomMessageTooLarge = this.isPhenomMessageTooLarge(body);

    //just for hindawi-play-events
    const sourceEvent = messageBody.event;
    if (messageBody.event.includes('hindawi-play-events')) {
      messageBody.event = messageBody.event.replace(
        'hindawi-play-events',
        'hindawi-review'
      );
    }

    const listeners = this.listeners[messageBody.event as string];

    if (!listeners?.length) {
      // this.logger.info('Message skipped (event=%s): no listeners', sourceEvent);
      return;
    }
    data = messageBody.data;
    if (messageBody.phenomMessageTooLarge) {
      // this.logger.info(`Event larger than 256k. Start processing event from S3`);
      try {
        data = JSON.parse(await this.largeEventService.get(messageBody.data));
      } catch (error) {
        // this.logger.warn(error, 'Error getting event from bucket (MessageId=%s)', msg.MessageId);
        return;
      }
    }

    data.publisherName = messageBody.extractedPublisher;
    const messageAttributes = this.getMessageAttributeKey(body);
    await Promise.all(
      listeners?.map((fn) => fn(data, body[messageAttributes]))
    );

    return;
  };

  private handleError = (error) => {
    console.error(error, 'Error on SQS consumer');
  };

  private handleProcessingError = (error) => {
    console.error(error, 'Processing error on SQS consumer');
  };

  private getObjectKey(object, keys: string[]) {
    let key;
    keys.forEach((k) => {
      if (object[k]) key = k;
    });
    return key;
  }

  getMessageAttributeKey(event: Event | Message) {
    const messageAttributesCases = ['MessageAttributes', 'messageAttributes'];
    return this.getObjectKey(event, messageAttributesCases);
  }

  isPhenomMessageTooLarge(event: Event | Message): boolean {
    const messageAttributeKey = this.getMessageAttributeKey(event);
    if (!event[messageAttributeKey]) return false;
    const phenomMessageTooLarge =
      event[messageAttributeKey]['PhenomMessageTooLarge'];
    if (!phenomMessageTooLarge) {
      return false;
    }

    const stringValueKey = this.getStringValueKey(phenomMessageTooLarge);
    return phenomMessageTooLarge[stringValueKey] === 'Y';
  }

  getStringValueKey(object: Record<string, any>) {
    const stringValueCases = ['stringValue', 'StringValue', 'Value'];
    return this.getObjectKey(object, stringValueCases);
  }
}
