import { MessageAttributeMap } from 'aws-sdk/clients/sns';

/**
 * Interface for Listerner that handles events of type MT
 */
export interface IListener<MT> {
  start(): void;

  stop(): void;

  handle<T extends keyof MT>(eventName: T, fn: (m: MT[T]) => Promise<void>): void;

  handleMap<T extends keyof MT>(eventsMap: Record<T, (m: MT[T]) => Promise<void>>): void;
}

export interface IPublisher<MT> {
  send<K extends keyof MT>(event: K, data: MT[K],  id?, timestamp?, MessageAttributes?: MessageAttributeMap): Promise<void>;
}

export interface ILargeEventService {
  put(body: string): Promise<string>;
  get(url: string): Promise<string>;
}



export interface QueueServiceOptions extends LargeEventsOptions, CodecOptions, SqsOptions, SnsOptions {
  defaultMessageAttributes?: string;
}

export interface SnsOptions extends AwsBaseOptions, CodecOptions {
  snsEndpoint: string;
  topicName: string;
}

export interface SqsOptions extends AwsBaseOptions, CodecOptions {
  sqsEndpoint: string;
  queueName?: string;
}

export interface AwsBaseOptions {
  region: string;
  accessKeyId: string;
  secretAccessKey: string;
}

export interface CodecOptions {
  eventNamespace?: string;
  publisherName?: string;
  serviceName?: string;
}

export interface LargeEventsOptions extends AwsBaseOptions {
  largeEventsBucket?: string,
  largeEventsPrefix?: string
}

export interface CreateQueueServiceResult {
  publishMessage({
                   event,
                   data,
                   messageAttributes,
                   timestamp,
                   id
                 });

  registerEventHandler({ event, handler }): void;

  start(): Promise<void>;

  stop(): Promise<void>;
}
