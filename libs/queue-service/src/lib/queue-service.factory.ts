import { Listener } from './listener.service';
import { LargeEventService } from './largeEvent.service';
import { CodecOptions, QueueServiceOptions, SnsOptions, SqsOptions } from './queue-service.contract';
import { MessageCodec } from './message.codec';
import { Publisher } from './publisher.service';
import { SNS, SQS } from 'aws-sdk';

export class QueueServiceFactory {

  private static getPrefixes(options: CodecOptions): string[] {
    return [`${options.eventNamespace}:${options.publisherName}:${options.serviceName}:`];
  }

  static getListener<T>(options: SqsOptions, prefixes?: string[], sqs?: SQS): Listener<T> {
    prefixes = prefixes || QueueServiceFactory.getPrefixes(options);

    sqs = sqs || new SQS({
      endpoint: options.sqsEndpoint,
      region: options.region,
      accessKeyId: options.accessKeyId,
      secretAccessKey: options.secretAccessKey
    });

    const largeEventService = new LargeEventService(options);
    const messageCodec = new MessageCodec(prefixes);

    return new Listener(sqs, largeEventService, messageCodec, options);
  }

  static async getTopicPublisher<T>(options: SnsOptions, prefixes?: string[], sns?:SNS): Promise<Publisher<T>> {
    prefixes = prefixes || QueueServiceFactory.getPrefixes(options);
    sns = sns || new SNS({
      endpoint: options.snsEndpoint,
      region: options.region,
      accessKeyId: options.accessKeyId,
      secretAccessKey: options.secretAccessKey
    });

    const largeEventService = new LargeEventService(options);
    const messageCodec = new MessageCodec(prefixes);

    const topicArn = await this.getTopicArn(sns, options.topicName);

    return new Publisher(sns, messageCodec, largeEventService, { topicArn });
  }

  static async getQueuePublisher<T>(options: SqsOptions, prefixes: string[], sqs?:SQS): Promise<Publisher<T>> {
    prefixes = prefixes || QueueServiceFactory.getPrefixes(options);

    const largeEventService = new LargeEventService(options);
    const messageCodec = new MessageCodec(prefixes);

    sqs = sqs || new SQS({
      endpoint: options.sqsEndpoint,
      region: options.region,
      accessKeyId: options.accessKeyId,
      secretAccessKey: options.secretAccessKey
    });

    return new Publisher(sqs, messageCodec, largeEventService, { queueName: options.queueName });
  }


  private static async getTopicArn(sns: SNS, topic: string) {
    try {
      const { TopicArn } = await sns
        .createTopic({
          Name: topic
        })
        .promise();

      return TopicArn;
    } catch (error) {
      console.warn(error, 'unable to get the arn of the topic(%s)', topic);
    }
  }

}
