import { SNS } from 'aws-sdk';
import { MessageTypes, MockLargeEventService } from './testUtils.spec';
import { MessageCodec } from '../message.codec';
import { Publisher } from '../publisher.service';


describe('QueueListener/Publisher', () => {
  let sns: SNS;
  let largeEventService: MockLargeEventService;
  const topic = 'topic1';
  const codec = new MessageCodec<MessageTypes>();

  beforeEach(() => {
    sns = Object.create(SNS.prototype);
    largeEventService = new MockLargeEventService();

    sns.publish = jest.fn().mockReturnValue({
      promise: jest.fn().mockResolvedValue(undefined)
    });
  });

  it('should send a message', async () => {

    const publisher = new Publisher<MessageTypes>(sns, codec, largeEventService, { topicArn: topic });

    await publisher.send('str', { payload: 'foo' });

    expect(sns.publish).toBeCalledWith(
      expect.objectContaining({
        TopicArn: topic,
        MessageAttributes: undefined
      })
    );
  });

  it('should upload the large event to the S3 store', async () => {
    const publisher = new Publisher<MessageTypes>(sns, codec, largeEventService, { topicArn: topic });
    largeEventService.put.mockResolvedValue('s3-bucket-id');

    const bigPayload = Array.from(
      { length: 500 },
      () => `Leverage agile frameworks to provide a robust synopsis for high level overviews. Iterative approaches to corporate strategy foster collaborative thinking to further the overall value proposition. Organically grow the holistic world view of disruptive innovation via workplace diversity and empowerment.

    Bring to the table win-win survival strategies to ensure proactive domination. At the end of the day, going forward, a new normal that has evolved from generation X is on the runway heading towards a streamlined cloud solution. User generated content in real-time will have multiple touchpoints for offshoring.

    Capitalize on low hanging fruit to identify a ballpark value added activity to beta test. Override the digital divide with additional clickthroughs from DevOps. Nanotechnology immersion along the information highway will close the loop on focusing solely on the bottom line.`
    );

    await publisher.send('str', { payload: JSON.stringify(bigPayload) });

    expect(largeEventService.put).toHaveBeenCalledTimes(1);
    expect(sns.publish).toBeCalledWith(
      expect.objectContaining({
        TopicArn: topic,
        MessageAttributes: {
          PhenomMessageTooLarge: {
            DataType: 'String',
            StringValue: 'Y'
          }
        }
      })
    );
  });
});
