import { MessageCodec } from '../message.codec';
import {
  createEvent,
  MessageTypes,
  MockLargeEventService,
  mockSqsConsumer,
  NumberMessage,
  StringMessage
} from './testUtils.spec';
import { Listener } from '../listener.service';
import { LargeEventsOptions, SqsOptions } from '@hindawi/queue-service';
import AWS from 'aws-sdk';

jest.mock('aws-sdk', () => {
  const SQSMocked = {
    sendMessage: jest.fn().mockReturnThis(),
    promise: jest.fn(),
    getQueueUrl: jest.fn().mockReturnValue({
      promise() {
        return Promise.resolve({ QueueUrl: 'queueUrl' });
      }
    })
  };
  return {
    SQS: jest.fn(() => SQSMocked)
  };
});


describe('QueueService/Listener', () => {
  let mockConsumer;
  let codec: MessageCodec<MessageTypes>;

  const sqs = new AWS.SQS({
    region: 'us-east-1'
  });


  beforeEach(() => {
    mockConsumer = mockSqsConsumer();
    codec = new MessageCodec<MessageTypes>();
  });

  afterEach(() => {
    mockConsumer.restore();
  });

  it('interacts with consumer properly', async () => {
    const listener = new Listener<MessageTypes>(sqs, undefined, codec, { queueName: 'test-queue' } as SqsOptions);
    await listener.start();

    expect(mockConsumer.spy).toBeCalled();
    expect(mockConsumer.consumer.start).toBeCalled();

    listener.stop();

    expect(mockConsumer.spy).toBeCalled();
    expect(mockConsumer.consumer.stop).toBeCalled();
  });

  it('should listen for an event', async () => {
    const listener = new Listener<MessageTypes>(sqs, undefined, codec, { queueName: 'test-queue' } as SqsOptions);
    await listener.start();

    const handleStr = jest.fn();
    const handleNum = jest.fn();

    listener.handle('str', handleStr);
    listener.handle('num', handleNum);

    await mockConsumer.simulateEvent(createEvent('str', { payload: 'foo' }));
    await mockConsumer.simulateEvent(createEvent('num', { value: 123 }));

    expect(handleStr).toHaveBeenCalledWith({ payload: 'foo', publisherName: undefined }, undefined);
    expect(handleStr).toBeCalledTimes(1);

    expect(handleNum).toBeCalledWith({ value: 123 }, undefined);
    expect(handleNum).toBeCalledTimes(1);
  });

  it('properly types listeners', () => {
    const listener = new Listener<MessageTypes>(sqs, undefined, codec, { queueName: 'test-queue' } as SqsOptions);


    listener.handle('str', async (_message: StringMessage) => void _message);
    listener.handle('num', async (_message: NumberMessage) => void _message);
  });

  it('should support multiple listeners for same event', async () => {
    const listener = new Listener<MessageTypes>(sqs, undefined, codec, { queueName: 'test-queue' } as SqsOptions);

    await listener.start();

    const handleFn1 = jest.fn().mockResolvedValue(undefined);
    const handleFn2 = jest.fn().mockResolvedValue(undefined);

    listener.handle('str', handleFn1);
    listener.handle('str', handleFn2);

    mockConsumer.simulateEvent(createEvent('str', { payload: 'foo' }));

    expect(handleFn1).toBeCalledWith({ payload: 'foo', publisherName: undefined }, undefined);
    expect(handleFn2).toBeCalledWith({ payload: 'foo', publisherName: undefined }, undefined);
  });

  it('should allow listening to multiple events', async () => {
    const listener = new Listener<MessageTypes>(sqs, undefined, codec, { queueName: 'test-queue' } as SqsOptions);

    await listener.start();

    const handleFn = jest.fn().mockResolvedValue(undefined);

    listener.handle(['str', 'stillStr'], handleFn);

    mockConsumer.simulateEvent(createEvent('str', { payload: 'foo' }));
    mockConsumer.simulateEvent(createEvent('stillStr', { payload: 'bar' }));

    expect(handleFn).toBeCalledWith({ payload: 'bar', publisherName: undefined }, undefined);
    expect(handleFn).toBeCalledWith({ payload: 'foo', publisherName: undefined }, undefined);
  });

  it('should pass the correct arguments to the event callback', async () => {
    const listener = new Listener<MessageTypes>(sqs, undefined, codec, { queueName: 'test-queue' } as SqsOptions);
    await listener.start();

    const handleStr = jest.fn();

    listener.handle('str', handleStr);

    mockConsumer.simulateEvent(
      createEvent(
        'str',
        { payload: 'foo' },
        {
          type: 'bar'
        }
      )
    );
    expect(handleStr).toHaveBeenCalledWith(
      { payload: 'foo', publisherName: undefined },
      {
        type: 'bar'
      }
    );
    expect(handleStr).toBeCalledTimes(1);
  });

  it('should get the publisher name from event\'s prefix', async () => {
    const codec = new MessageCodec<MessageTypes>(['pen:foo']);
    const listener = new Listener<MessageTypes>(sqs, undefined, codec, { queueName: 'test-queue' } as SqsOptions);
    await listener.start();

    const handleStr = jest.fn();

    listener.handle(':JournalUpdated', handleStr);

    mockConsumer.simulateEvent(
      createEvent(
        'pen:foo:JournalUpdated',
        { payload: 'foo' },
        {
          type: 'bar'
        }
      )
    );

    expect(handleStr).toHaveBeenCalledWith(
      { payload: 'foo', publisherName: 'foo' },
      {
        type: 'bar'
      }
    );
    expect(handleStr).toBeCalledTimes(1);
  });


  it('should handle case when property comes as messageAttributes', async () => {
    const listener = new Listener<MessageTypes>(sqs, undefined, codec, { queueName: 'test-queue' } as SqsOptions);
    await listener.start();

    const handleStr = jest.fn();

    listener.handle('str', handleStr);

    const event = {
      Body: JSON.stringify({
        event: 'str',
        data: { payload: 'foo' },
        messageAttributes: {
          type: 'bar'
        }
      })
    };
    mockConsumer.simulateEvent(event);
    expect(handleStr).toHaveBeenCalledWith(
      { payload: 'foo', publisherName: undefined },
      {
        type: 'bar'
      }
    );
    expect(handleStr).toBeCalledTimes(1);
  });

  it('should handle case when property comes as MessageAttributes', async () => {
    const listener = new Listener<MessageTypes>(sqs, undefined, codec, { queueName: 'test-queue' } as SqsOptions);
    await listener.start();

    const handleStr = jest.fn();

    listener.handle('str', handleStr);

    const event = {
      Body: JSON.stringify({
        event: 'str',
        data: { payload: 'foo' },
        MessageAttributes: {
          type: 'bar'
        }
      })
    };
    mockConsumer.simulateEvent(event);
    expect(handleStr).toHaveBeenCalledWith(
      { payload: 'foo', publisherName: undefined },
      {
        type: 'bar'
      }
    );
    expect(handleStr).toBeCalledTimes(1);
  });

  it('should handle case when property from PhenomMessageTooLargecomes comes as stringValue', async () => {

    const largeEventService = new MockLargeEventService();
    const listener = new Listener<MessageTypes>(sqs, largeEventService, codec, { queueName: 'test-queue' } as SqsOptions);
    await listener.start();

    const handleStr = jest.fn();

    listener.handle('str', handleStr);

    const event = {
      Body: JSON.stringify({
        event: 'str',
        data: { payload: 'foo' },
        MessageAttributes: {
          PhenomMessageTooLarge: { stringValue: 'bar' }
        }
      })
    };
    mockConsumer.simulateEvent(event);
    expect(handleStr).toHaveBeenCalledWith(
      { payload: 'foo', publisherName: undefined },
      {
        PhenomMessageTooLarge: { stringValue: 'bar' }
      }
    );
    expect(handleStr).toBeCalledTimes(1);
  });

  it('should handle case when property from PhenomMessageTooLargecomes comes as StringValue', async () => {
    const largeEventService = new MockLargeEventService();
    const listener = new Listener<MessageTypes>(sqs, largeEventService, codec, { queueName: 'test-queue' } as SqsOptions);
    await listener.start();

    const handleStr = jest.fn();

    listener.handle('str', handleStr);

    const event = {
      Body: JSON.stringify({
        event: 'str',
        data: { payload: 'foo' },
        MessageAttributes: {
          PhenomMessageTooLarge: { StringValue: 'bar' }
        }
      })
    };
    mockConsumer.simulateEvent(event);
    expect(handleStr).toHaveBeenCalledWith(
      { payload: 'foo', publisherName: undefined },
      {
        PhenomMessageTooLarge: { StringValue: 'bar' }
      }
    );
    expect(handleStr).toBeCalledTimes(1);
  });
  it('should handle case when property from PhenomMessageTooLargecomes comes as Value', async () => {
    const largeEventService = new MockLargeEventService();
    const listener = new Listener<MessageTypes>(sqs, largeEventService, codec, { queueName: 'test-queue' } as SqsOptions);
    await listener.start();

    const handleStr = jest.fn();

    listener.handle('str', handleStr);

    const event = {
      Body: JSON.stringify({
        event: 'str',
        data: { payload: 'foo' },
        MessageAttributes: {
          PhenomMessageTooLarge: { Value: 'bar' }
        }
      })
    };
    mockConsumer.simulateEvent(event);
    expect(handleStr).toHaveBeenCalledWith(
      { payload: 'foo', publisherName: undefined },
      {
        PhenomMessageTooLarge: { Value: 'bar' }
      }
    );
    expect(handleStr).toBeCalledTimes(1);
  });
});
