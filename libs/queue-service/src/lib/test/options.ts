import { QueueServiceOptions } from '@hindawi/queue-service';

export const testOptions = {
  region: 'eu-east-1',
  // accessKeyId: process.env.accessKeyId,
  // secretAccessKey: process.env.secretAccessKey,

  snsEndpoint: 'http://localhost:9911',
  sqsEndpoint: 'http://localhost:9324',
  // s3Endpoint: process.env.qs_s3_endpoint,

  topicName: 'test1',
  queueName: 'backend-queue',

  largeEventsBucket: 'gaia-dev',

  eventNamespace: 'pen',
  publisherName: 'hindawi',
  serviceName: 'production',
  defaultMessageAttributes: undefined
} as QueueServiceOptions;

export const testOptionsWorker = {
  region: 'eu-east-1',
  // accessKeyId: process.env.accessKeyId,
  // secretAccessKey: process.env.secretAccessKey,

  snsEndpoint: 'http://localhost:9911',
  sqsEndpoint: 'http://localhost:9324',
  // s3Endpoint: process.env.qs_s3_endpoint,

  // topicName: 'test1',
  queueName: 'test-queue',

  largeEventsBucket: 'gaia-dev',

  eventNamespace: 'pen',
  publisherName: 'hindawi',
  serviceName: 'production',
  defaultMessageAttributes: undefined
} as QueueServiceOptions;
