import { MessageCodec } from '../message.codec';
import { MessageTypes } from './testUtils.spec';

describe('QueueListener/MessageCodec', () => {
  it('should decode values', () => {
    const codec = new MessageCodec<MessageTypes>();

    const input = '{"event": "str", "data": { "payload": "foo"}}';
    const { event, data } = codec.decode(input);

    expect(event).toBe('str');
    expect(data).toEqual({ payload: 'foo' });
  });

  it('should encode values', () => {
    const codec = new MessageCodec<MessageTypes>();

    const output = codec.encode('str', { payload: 'foo' }, 'event-id', 'now');

    expect(output).toMatchInlineSnapshot(
      `"{\\"id\\":\\"event-id\\",\\"data\\":{\\"payload\\":\\"foo\\"},\\"event\\":\\"str\\",\\"timestamp\\":\\"now\\"}"`,
    );
  });

  it('should achieve codec symetry', () => {
    const codec = new MessageCodec<MessageTypes>();

    const out = codec.encode('str', { payload: 'foo' }, 'event-id', 'now');
    expect(codec.decode(out)).toEqual({
      id: 'event-id',
      data: { payload: 'foo' },
      event: 'str',
      timestamp: 'now',
    });

    const input = JSON.stringify({
      id: 'event-id',
      data: { payload: 'foo' },
      event: 'str',
      timestamp: 'now',
    });
    expect(codec.encode('str', { payload: 'foo' }, 'event-id', 'now')).toBe(input);
  });

  describe('prefix support', () => {
    test('should strip prefix from event name when decoding', () => {
      const codec = new MessageCodec<MessageTypes>(['foo:']);

      const message = JSON.stringify({ event: 'foo:str', data: { payload: 'foo' } });
      const { event, data } = codec.decode(message);

      expect(event).toBe('str');
      expect(data).toEqual({ payload: 'foo' });
    });

    test('throws error on decode if message type doesnt start with prefix', () => {
      const codec = new MessageCodec<MessageTypes>(['foo:']);

      const message = JSON.stringify({ event: 'str', data: { payload: 'foo' } });

      expect(() => codec.decode(message)).toThrowErrorMatchingInlineSnapshot(
        `"Message type(str) doesn't start with any of the prefixes(foo:)"`,
      );
    });

    test('adds prefix when encoding messages', () => {
      const codec = new MessageCodec<MessageTypes>(['foo:']);

      const output = codec.encode('str', { payload: 'foo' }, 'event-id', 'now');

      expect(output).toMatchInlineSnapshot(
        `"{\\"id\\":\\"event-id\\",\\"data\\":{\\"payload\\":\\"foo\\"},\\"event\\":\\"foo:str\\",\\"timestamp\\":\\"now\\"}"`,
      );
    });

    test('still achieves codec symetry', () => {
      const codec = new MessageCodec<MessageTypes>(['foo:']);

      const out = codec.encode('str', { payload: 'foo' }, 'event-id', 'now');
      expect(codec.decode(out)).toEqual({
        id: 'event-id',
        data: { payload: 'foo' },
        event: 'str',
        timestamp: 'now',
        extractedPublisher: null,
      });

      const input = JSON.stringify({
        id: 'event-id',
        data: { payload: 'foo' },
        event: 'foo:str',
        timestamp: 'now',
      });
      expect(codec.encode('str', { payload: 'foo' }, 'event-id', 'now')).toBe(input);
    });
  });
});
