import { Consumer, ConsumerOptions } from 'sqs-consumer';
import { Message } from 'aws-sdk/clients/sqs';
import { SQS } from 'aws-sdk';
import { ILargeEventService } from '@hindawi/queue-service';

export class MockConsumer {
  public on = jest.fn();
  public start = jest.fn();
  public stop = jest.fn();
  public sqs: SQS;
  public queueUrl: string;
  // eslint-disable-next-line @typescript-eslint/ban-types
  public handleMessage: Function;


  constructor({ sqs, queueUrl, handleMessage }: ConsumerOptions) {
    this.sqs = sqs;
    this.queueUrl = queueUrl;
    this.handleMessage = handleMessage;
  }

  simulateEvent(m: Message) {
    this.handleMessage(m);
  }
}

export function mockSqsConsumer() {
  let mockedConsumer: MockConsumer;

  const spy = jest.spyOn(Consumer, 'create').mockImplementation(opts => {
    mockedConsumer = new MockConsumer(opts);
    return (mockedConsumer as unknown) as Consumer;
  });

  return {
    spy,
    get consumer() {
      return mockedConsumer;
    },
    restore() {
      spy.mockRestore();
    },
    simulateEvent(m: Message) {
      mockedConsumer.simulateEvent(m);
    }
  };
}

export function createEvent(event: string, data: object, attributes?: object): Message {
  if (!attributes) {
    return {
      Body: JSON.stringify({
        event,
        data
      })
    };
  }

  return {
    Body: JSON.stringify({
      event,
      data,
      MessageAttributes: {
        ...attributes
      }
    })
  };
}

export interface StringMessage {
  payload: string;
}

export interface NumberMessage {
  value: number;
}

export interface MessageTypes {
  str: StringMessage;
  stillStr: StringMessage;
  num: NumberMessage;
  [':JournalUpdated']: NumberMessage;
}

export class MockLargeEventService implements ILargeEventService {
  put = jest.fn();
  get = jest.fn();
}
//todo - this is to bypass "all test related files must have a test"
describe('QueueListener/Publisher', () => {
  it('works', async () => {
    expect(true);
  })
});

