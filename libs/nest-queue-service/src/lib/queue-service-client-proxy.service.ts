import { ClientProxy, ReadPacket, WritePacket } from '@nestjs/microservices';
import { Inject, Injectable, Logger, NotImplementedException } from '@nestjs/common';
import { QueueServiceWrapper } from './queue-service.wrapper';

/**
 * Custom transport client, in order to use NestJs's mmicroservices with AWS SNS/SQS
 */
@Injectable()
export class QueueServiceClientProxy extends ClientProxy {
  private messageQueue: Promise<any>;

  constructor(
    @Inject(QueueServiceWrapper)
    private readonly queueServiceProvider?: QueueServiceWrapper,
  ) {
    super();
    this.messageQueue = queueServiceProvider.messageQueue;
  }

  close(): any {
    Logger.log('Client proxy close', 'QueueServiceClientProxy');
  }

  async connect(): Promise<void> {
    Logger.log('Client proxy connect', 'QueueServiceClientProxy');
  }

  protected async dispatchEvent<T = any>(packet: ReadPacket): Promise<T> {
    return (await this.messageQueue).publishMessage({
      event: packet.pattern,
      data: packet.data,
    });
  }

  protected publish(packet: ReadPacket, callback: (packet: WritePacket) => void): Function {
    throw new NotImplementedException('Publish not implemented yet');
  }
}
