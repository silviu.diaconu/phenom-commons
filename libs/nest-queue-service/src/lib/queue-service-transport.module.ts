import { Module, Type } from '@nestjs/common';
import { QueueServiceClientProxy } from './queue-service-client-proxy.service';
import { QueueServiceTransportServer } from './queue-service-transport-server.service';
import {
  QUEUE_SERVICE_OPTIONS_TOKEN,
  QueueServiceOptions,
  QueueServiceWrapper,
} from './queue-service.wrapper';
import { DynamicModule } from '@nestjs/common/interfaces/modules/dynamic-module.interface';
import { ModuleMetadata } from '@nestjs/common/interfaces';

export interface SnsNestTransportModuleAsyncOptions extends Pick<ModuleMetadata, 'imports'> {
  useFactory: (...args: any[]) => Promise<QueueServiceOptions> | QueueServiceOptions;
  inject?: any[];
}

export interface QueueServiceOptionsFactory {
  createQueueServiceOptions(): Promise<QueueServiceOptions> | QueueServiceOptions;
}

@Module({})
export class QueueServiceTransportModule {
  static forRootAsync(options: SnsNestTransportModuleAsyncOptions): DynamicModule {
    return {
      imports: [],
      module: QueueServiceTransportModule,
      providers: [
        {
          provide: QUEUE_SERVICE_OPTIONS_TOKEN,
          useFactory: options.useFactory,
          inject: options.inject || [],
        },
        QueueServiceWrapper,
        QueueServiceClientProxy,
        QueueServiceTransportServer,
      ],
      exports: [QueueServiceClientProxy, QueueServiceTransportServer],
    };
  }
}
