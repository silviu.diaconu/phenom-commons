import { CustomTransportStrategy, Server } from '@nestjs/microservices';
import { Inject, Injectable } from '@nestjs/common';
import { QueueServiceWrapper } from './queue-service.wrapper';

@Injectable()
export class QueueServiceTransportServer extends Server implements CustomTransportStrategy {
  private readonly messageQueue: Promise<any>;

  constructor(
    @Inject(QueueServiceWrapper)
    private readonly queueServiceProvider?: QueueServiceWrapper,
  ) {
    super();
    this.messageQueue = queueServiceProvider.messageQueue;
  }

  close(): void {
    // this.sqsConsumer.stop();
  }

  async listen(callback: () => void): Promise<any> {
    const messageQueue = await this.messageQueue;
    this.registerEventHandlers(messageQueue);
    return this.start(messageQueue, callback);
  }

  private start(messageQueue, callback): void {
    messageQueue.start();
    callback();
  }

  private registerEventHandlers(messageQueue) {
    // eslint-disable-next-line @typescript-eslint/no-this-alias
    const _this = this;

    for (const event of this.messageHandlers.keys()) {
      messageQueue.registerEventHandler({
        event: event,
        handler: function (message) {
          _this.logger.debug('Handling ... ' + event, 'QueueServiceTransportServer');

          return _this.handleEvent(event, { pattern: event, data: message }, null);
        },
      });
      this.logger.debug(`Registered new subscription "${event}"`, 'QueueServiceTransportServer');
    }
  }
}
