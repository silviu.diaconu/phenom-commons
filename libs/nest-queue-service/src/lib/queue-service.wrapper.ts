import { createQueueService } from '@hindawi/queue-service';
import { Inject, Injectable } from '@nestjs/common';

export const QUEUE_SERVICE_OPTIONS_TOKEN = 'QUEUE_SERVICE_OPTIONS_TOKEN';

export declare type QueueServiceOptions = {
  region: string;
  accessKeyId: string;
  secretAccessKey: string;

  snsEndpoint: string;
  sqsEndpoint: string;
  s3Endpoint?: string;

  topicName: string;
  queueName: string;
  bucketName?: string;
  bucketPrefix?: string;

  eventNamespace?: string;
  publisherName?: string;
  serviceName?: string;
  defaultMessageAttributes?: string;
};

@Injectable()
export class QueueServiceWrapper {
  public messageQueue: Promise<any>;

  constructor(
    @Inject(QUEUE_SERVICE_OPTIONS_TOKEN)
    private readonly queueServiceOptions?: QueueServiceOptions,
  ) {
    this.messageQueue = createQueueService(queueServiceOptions);
  }
}
