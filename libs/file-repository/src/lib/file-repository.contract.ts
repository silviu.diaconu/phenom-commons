import { Readable } from 'stream';

export interface FileDescriptor {
  path: string;
}

export interface UploadRequest extends FileDescriptor {
  source: string | Readable;
}

export interface FileRepository {
  delete(request: FileDescriptor): Promise<boolean>;
  download(request: FileDescriptor): Promise<Readable>;
  list(request: FileDescriptor): Promise<FileDescriptor[]>,
  upload(request: UploadRequest): Promise<void>;

}
