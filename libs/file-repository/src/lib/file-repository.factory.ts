import { FileRepository } from '@hindawi/file-repository';
import { S3FileRepository } from './s3-file-repository.service';
import { S3ClientConfig } from '@aws-sdk/client-s3';
import { FtpFileRepository } from './ftp-file-repository.service';
import { AccessOptions } from 'basic-ftp';

export class FileRepositoryFactory {
  static s3(s3Config: S3ClientConfig, bucket: string): FileRepository {
    return new S3FileRepository(s3Config, bucket);
  }

  static ftp(fptConfig: AccessOptions): FileRepository {
    return new FtpFileRepository(fptConfig);
  }
}
