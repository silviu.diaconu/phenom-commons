import { Readable } from 'stream';

import { FileDescriptor, FileRepository, UploadRequest } from '@hindawi/file-repository';
import { paginateListObjectsV2, S3, S3ClientConfig } from '@aws-sdk/client-s3';

export class S3FileRepository implements FileRepository {
  private readonly s3: S3;

  constructor(private readonly s3Config: S3ClientConfig, private readonly bucket: string) {
    this.s3 = new S3(s3Config);
  }

  async delete(request: FileDescriptor): Promise<boolean> {
    const params = { Bucket: this.bucket, Key: request.path };

    try {
      await this.s3.headObject(params);
      console.log('File Found in S3');
      try {
        await this.s3.deleteObject(params);
        console.log('file deleted Successfully');

        return true;
      } catch (err) {
        console.log('ERROR in file Deleting : ' + JSON.stringify(err));
        return false;
      }
    } catch (err) {
      console.log('File not Found ERROR : ' + err.message);
      return false;
    }
  }

  O;

  async download(request: FileDescriptor): Promise<Readable> {
    const response = await this.s3.getObject({
      Bucket: this.bucket,
      Key: request.path,
    });
    if (!(response.Body instanceof Readable)) {
      throw new Error(
        'Did not get a correct stream back from the AWS SDK. Something weird happened!',
      );
    }
    return response.Body;
  }

  async upload(request: UploadRequest): Promise<void> {
    return this.s3
      .putObject({
        Body: request.source,
        Bucket: this.bucket,
        Key: request.path,
      })
      .then();
  }

  async list(request: FileDescriptor): Promise<FileDescriptor[]> {
    const objects = await this.s3.listObjects({
      Bucket: this.bucket,
      Prefix: request.path,
    });

    if (objects.Contents) {
      return objects.Contents.map((file) => {
        return { path: file.Key } as FileDescriptor;
      });
    }
    return [];
  }
}
