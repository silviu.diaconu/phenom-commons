import { Client, enterPassiveModeIPv4, AccessOptions, FTPResponse } from 'basic-ftp';
import { Readable, Writable } from 'stream';

export const FTP_KEEP_ALIVE = 3 * 1000;
export const FTP_TIMEOUT = 300 * 1000;


export class FtpClient {
  private ftpClient: Client;
  private isIdle = true;
  private keepAliveTimer;
  constructor(timeout = FTP_TIMEOUT, isPassiveMode = false) {
    this.ftpClient = new Client(timeout);
    if (isPassiveMode) {
      this.ftpClient.prepareTransfer = enterPassiveModeIPv4;
    }
  }
  public async access(opts: AccessOptions): Promise<FTPResponse> {
    const typedOptions: AccessOptions = {
      ...opts,
    };

    const connection = await this.ftpClient.access(typedOptions);

    // https://docs.aws.amazon.com/elasticloadbalancing/latest/network/network-load-balancers.html
    // https://stackoverflow.com/questions/42816884/tcp-keep-alive-parameters-not-being-honoured
    this.ftpClient.ftp.socket.setKeepAlive(true, FTP_KEEP_ALIVE);

    this.keepAliveTimer = setInterval(async () => {
      if (this.ftpClient.closed) {
        clearInterval(this.keepAliveTimer);
        return;
      }
      if (this.isIdle) {
        this.isIdle = false;
        await this.ftpClient.send('NOOP');
        this.isIdle = true;
      }
    }, 1 * 60 * 1000);

    return connection;
  }
  public async uploadFrom(fileStream: Readable, remotePath: string) {
    return this.execute(this.ftpClient.uploadFrom, fileStream, remotePath);
  }

  public async ensureDir(path: string) {
    return this.execute(this.ftpClient.ensureDir, path);
  }

  public async list(dirPath: string) {
    return this.execute(this.ftpClient.list, dirPath);
  }

  public cd(path: string) {
    return this.execute(this.ftpClient.cd, path);
  }

  public download(destination: string | Writable, fromRemotePath: string) {
    return this.execute(this.ftpClient.downloadTo, destination, fromRemotePath);
  }

  public delete(path: string) {
    return this.execute(this.ftpClient.remove, path);
  }

  public close(): void {
    clearInterval(this.keepAliveTimer); // jest throws warnings when its process finishes with intervals not cleared
    return this.ftpClient.close();
  }

  private async execute<TArgs extends unknown[], TReturn>(
    cb: (...params: TArgs) => Promise<TReturn>,
    ...args: TArgs
  ): Promise<TReturn> {
    this.isIdle = false;
    const cbFunc: (...params: TArgs) => Promise<TReturn> = cb.bind(this.ftpClient);
    const result = await cbFunc(...args);
    this.isIdle = true;
    return result;
  }
}
