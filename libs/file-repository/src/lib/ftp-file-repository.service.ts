import { FileDescriptor, FileRepository, UploadRequest } from '@hindawi/file-repository';
import { Readable, Transform } from 'stream';
import { AccessOptions, FileInfo } from 'basic-ftp';
import { dirname } from 'path';
import { FtpClient } from './ftp-client.service';

export class FtpFileRepository implements FileRepository {
  ftpConfig: AccessOptions;
  client: FtpClient;

  constructor(ftpConfig: AccessOptions) {
    this.ftpConfig = ftpConfig;
    this.client = new FtpClient();
  }

  async delete(request: FileDescriptor): Promise<boolean> {
    try {
      await this.client.access(this.ftpConfig);
      const deleteRes = await this.client.delete(request.path);
      this.client.close();
    } catch (e) {
      console.log('Error when deleting ' + e.path + '. ->' + e.message);
      return false;
    }
    return true;
  }

  async download(request: FileDescriptor): Promise<Readable> {
    await this.client.access(this.ftpConfig);

    // Create Stream, Writable AND Readable
    const inoutStream = new Transform({
      transform(chunk, encoding, callback) {
        this.push(chunk);
        callback();
      },
    });

    const downloadRes = await this.client.download(inoutStream, request.path);
    this.client.close();
    return inoutStream;
  }

  async list(request: FileDescriptor): Promise<FileDescriptor[]> {
    await this.client.access(this.ftpConfig);
    const listRes: FileInfo[] = await this.client.list(request.path);
    this.client.close();

    return listRes.map((file) => {
      return { path: file.name } as FileDescriptor;
    });
  }

  async upload(request: UploadRequest): Promise<void> {
    await this.client.access(this.ftpConfig);
    await this.client.ensureDir(dirname(request.path));
    const uploadRes = await this.client.uploadFrom(request.source as Readable, request.path);
    this.client.close();
  }
}
