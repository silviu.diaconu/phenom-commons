import { Test, TestingModule } from '@nestjs/testing';

import { localFilePath, fileName, streamToString } from './utils';
import * as fs from 'fs';
import { FileRepository, FileRepositoryFactory } from '@hindawi/file-repository';
import { Readable } from 'stream';

describe('S3FileRepository', () => {
  let moduleRef: TestingModule;
  let service: FileRepository;

  beforeAll(async () => {
    moduleRef = await Test.createTestingModule({
      imports: [],
      providers: [
        {
          provide: 'S3FileRepository',
          useFactory: () => {
            return FileRepositoryFactory.s3(
              {
                credentials: {
                  accessKeyId: 'key',
                  secretAccessKey: 'secret',
                },
                region: process.env.s3region,
                endpoint: process.env.s3endpoint,
              },
              process.env.s3bucket,
            );
          },
        },
      ],
    }).compile();

    service = await moduleRef.get<FileRepository>('S3FileRepository');
  });

  describe('list empty repository', () => {
    it('should have 0 entries', async () => {
      const fileDescriptors = await service.list({ path: '' });

      expect(fileDescriptors.length).toBe(0)
    });
  });

  describe('upload', () => {
    it('correct file => should work', async () => {
      const testfile1 = fs.createReadStream(localFilePath);


      console.log('testfile loaded');
      await service.upload({
        path: fileName,
        source: testfile1,
      });

      testfile1.close();
      expect(true);
    });

      it('file with no name => should throw', async () => {
        const testfile1 = fs.createReadStream(localFilePath);

        await expect(
          service.upload({
            path: undefined,
            source: testfile1,
          }),
        ).rejects.toThrow('No value provided for input HTTP label: Key.');
      });
    });

    describe('download', () => {
      it('existing file => should work', async () => {
        const readable: Readable = await service.download({
          path: 'testfile1.txt',
        });

        const fileContent = await streamToString(readable);
        const expected = await streamToString(fs.createReadStream(localFilePath));
        expect(fileContent).toEqual(expected);
      });

      it('nonexistent file => should throw', async () => {
        await expect(service.download({ path: 'notexisting' })).rejects.toThrow('NoSuchKey');
      });
    });

    describe('list after download', () => {
      it('should have one entry', async () => {
        const fileDescriptors = await service.list({ path: '' });

        expect(fileDescriptors.length).toBe(1)
        expect(fileDescriptors).toContainEqual({ path: 'testfile1.txt' });
      });
    });

    describe('delete', () => {
      it('existing file => should return true', async () => {
        const deleted = await service.delete({ path: 'testfile1.txt' });
        expect(deleted).toEqual(true);
      });

      it('nonexisting file => should return false', async () => {
        const deleted = await service.delete({ path: 'testfile1.txt' });
        expect(deleted).toEqual(false);
      });

      it('invalid key => should return false', async () => {
        let deleted = await service.delete({ path: undefined });
        expect(deleted).toEqual(false);

        deleted = await service.delete({ path: null });
        expect(deleted).toEqual(false);

        deleted = await service.delete({ path: '' });
        expect(deleted).toEqual(false);
      });
    });
    describe('list after delete', () => {
      it('should have 0 entries', async () => {
        const fileDescriptors = await service.list({ path: '' });

        expect(fileDescriptors.length).toBe(0)
      });
  });
});
