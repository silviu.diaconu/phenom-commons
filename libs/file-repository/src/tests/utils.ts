import { readdirSync, unlinkSync } from 'fs';
import { join, dirname } from 'path';

export const fileName = 'testfile1.txt';
export const ftpFilePath = '/home/hindawi/test1/' + fileName;
export const localFilePath = './libs/file-repository/src/tests/test-files/' + fileName;
export const downloadedLocalFile = './libs/file-repository/src/tests/test-files/temp/download.txt';

export function clearTempFolder() {
  const dirPath = dirname(downloadedLocalFile);
  const tempFiles = readdirSync(dirPath);
  for (const file of tempFiles) {
    unlinkSync(join(dirPath, file));
  }
}
export function streamToString (stream) {
  const chunks = []
  return new Promise((resolve, reject) => {
    stream.on('data', chunk => chunks.push(chunk))
    stream.on('error', reject)
    stream.on('end', () => resolve(Buffer.concat(chunks).toString('utf8')))
  })
}
