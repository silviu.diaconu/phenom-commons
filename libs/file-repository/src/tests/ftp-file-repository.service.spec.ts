import { Test, TestingModule } from '@nestjs/testing';
import { FileRepository, FileRepositoryFactory } from '@hindawi/file-repository';
import * as fs from 'fs';
import { fileName, ftpFilePath, localFilePath, streamToString } from './utils';
import { dirname } from 'path';

describe('FtpFileRepository', () => {
  let moduleRef: TestingModule;
  let service: FileRepository;

  beforeAll(async () => {
    moduleRef = await Test.createTestingModule({
      imports: [],
      providers: [
        {
          provide: 'FtpFileRepository',
          useFactory: () => {
            return FileRepositoryFactory.ftp({
              host: process.env.host,
              port: parseInt(process.env.port),
              user: process.env.user,
              password: process.env.password,
            });
          },
        },
      ],
    }).compile();

    service = await moduleRef.get<FileRepository>('FtpFileRepository');
  });

  describe('should UPLOAD file ', () => {
    it('sucess', async () => {
      const testfile1 = fs.createReadStream(localFilePath);

      await service.upload({ source: testfile1, path: ftpFilePath });
      const fileList = await service.list({ path: dirname(ftpFilePath) });

      expect(fileList.map((fileEntry) => fileEntry.path).includes(fileName)).toBe(true);
    });
  });

  describe('should DOWNLOAD file ', () => {
    it('sucess', async () => {
      const readable = await service.download({
        path: ftpFilePath,
      });

      const fileContent = await streamToString(readable);
      const expected = await streamToString(fs.createReadStream(localFilePath));
      expect(fileContent).toEqual(expected);
    });
  });

  describe('should DELETE file ', () => {
    it('sucess', async () => {
      await service.delete({ path: ftpFilePath });

      const fileList = await service.list({ path: dirname(ftpFilePath) });
      expect(fileList.map((fileEntry) => fileEntry.path).includes(fileName)).toBe(false);
    });
  });
});
