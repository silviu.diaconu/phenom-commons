import { ArchiveFormat, PackageCreatorService } from '@hindawi/package-creator';
import { ArchiverService } from '../lib/package-creator.service';
import fs from 'fs';
import { IArchiver } from '../lib/archiver.contract';

describe('PackageCreatorService', () => {
  afterAll(() => {
    fs.unlink(__dirname + '/fixtures/archive.zip', () => null);
  });

  it('Should create archiver instance', () => {
    const archiver = PackageCreatorService.generateArchive(ArchiveFormat.ZIP);
    expect(archiver).toBeInstanceOf(ArchiverService);
  });

  describe('.pipe', () => {
    it('should take a write stream and pipe to it', async () => {
      const archiveName = 'archive.zip';
      const dirPath = __dirname + '/fixtures/';
      const archiver = PackageCreatorService.generateArchive(ArchiveFormat.ZIP);
      const output = fs.createWriteStream(dirPath + archiveName);
      await archiver.finalize();
      const dirStruct = fs.readdirSync(dirPath);

      expect(dirStruct).toContain(archiveName);
    });
  });

  describe('.append', () => {
    let fileName: string;
    let archiver: IArchiver;
    const bufferEntries = {};

    beforeAll(async () => {
      fileName = 'my-file.txt';
      archiver = PackageCreatorService.generateArchive(ArchiveFormat.ZIP);
      const file = fs.createReadStream(__dirname + '/fixtures/file.txt');
      archiver.append({ stream: file, name: fileName });
      archiver.on().data((data) => {
        bufferEntries[data.toString('utf8')] = data.toString('utf8');
      });
      await archiver.finalize();
    });
    it('Should append file to zip', async () => {
      expect(bufferEntries[fileName]).toEqual(fileName);
    });

    it('Should append the entire content of the file', async () => {
      const fileContent = fs
        .readFileSync(__dirname + '/fixtures/file.txt')
        .toString();
      expect(bufferEntries[fileContent]).toEqual(fileContent);
    });
  });

  describe('.directory', () => {
    const directoryPath = __dirname + '/fixtures/directory';
    let bufferEntries = {};
    let archiver: IArchiver;

    beforeEach(async () => {
      archiver = PackageCreatorService.generateArchive(ArchiveFormat.ZIP);
      const output = fs.createWriteStream(__dirname + '/fixtures/archive.zip');
      archiver.on().data((data) => {
        const entry = data.toString('utf8');
        bufferEntries[entry] = entry;
      });
      archiver.pipe(output);
    });

    afterEach(() => {
      bufferEntries = {};
    });

    it('should copy a directory and its content in a archive subdirectory', async () => {
      const archiveDirectoryName = 'newDir';
      const dirStruct = fs
        .readdirSync(directoryPath)
        .map((el) =>
          fs.lstatSync(`${directoryPath}/${el}`).isDirectory()
            ? `${archiveDirectoryName}/${el}/`
            : `${archiveDirectoryName}/${el}`
        );

      archiver.directory(`${directoryPath}/`, archiveDirectoryName);

      await archiver.finalize();

      dirStruct.forEach((e) => {
        expect(bufferEntries[e]).toEqual(e);
      });
    });

    it('should copy a directory and its contents in the archive root', async () => {
      const dirStruct = fs
        .readdirSync(directoryPath)
        .map((el) =>
          fs.lstatSync(`${directoryPath}/${el}`).isDirectory()
            ? `${el}/`
            : `${el}`
        );

      archiver.directory(`${directoryPath}/`, false);

      await archiver.finalize();

      dirStruct.forEach((e) => {
        expect(bufferEntries[e]).toEqual(e);
      });
    });
  });

  describe('.appendAll', () => {
    it('should append a list of files', async () => {
      const bufferEntries = {};
      const firstFile = 'file1.txt';
      const secondFile = 'file2.txt';
      const archiver = PackageCreatorService.generateArchive(ArchiveFormat.ZIP);
      const firstFileStream = fs.createReadStream(
        __dirname + '/fixtures/directory/' + firstFile
      );
      const secondFileStream = fs.createReadStream(
        __dirname + '/fixtures/directory/' + secondFile
      );

      archiver.appendAll(
        { stream: firstFileStream, name: firstFile },
        { stream: secondFileStream, name: secondFile }
      );

      archiver.on().data((data) => {
        const entry = data.toString('utf8');
        bufferEntries[entry] = entry;
      });

      await archiver.finalize();

      expect(bufferEntries[firstFile]).toEqual(firstFile);
      expect(bufferEntries[secondFile]).toEqual(secondFile);
    });
  });
});
