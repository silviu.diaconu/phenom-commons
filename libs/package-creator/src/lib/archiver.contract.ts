import * as stream from 'stream';
import { ArchiverServiceScopedEvents } from './package-creator.service';

export enum ArchiveFormat {
  ZIP = 'zip',
  TAR = 'tar',
}

export interface File {
  stream: stream.Readable;
  name: string;
}

export interface IArchiver {
  stream: stream.Transform;
  finalize(): Promise<void>;
  append(file: File): void;
  directory(sourcePath: string, destinationPath: string | boolean): void;
  pipe(stream: stream.Writable): void;
  appendAll(...files: File[]): void;
  on(): ArchiverServiceScopedEvents;
}
