import archiver, { Archiver } from 'archiver';
import * as stream from 'stream';
import { ArchiveFormat, IArchiver, File } from './archiver.contract';

enum PackageCreatorEvent {
  ERROR = 'error',
  WARNING = 'warning',
  DATA = 'data',
  PROGRESS = 'progress',
  CLOSE = 'close',
  FINISH = 'finish',
}

interface ProgressData {
  entries: {
    total: number;
    processed: number;
  };
  fs: {
    totalBytes: number;
    processedBytes: number;
  };
}

export class ArchiverServiceScopedEvents {
  constructor(private archive: Archiver) {}

  error(listener: (error: Error) => void): void {
    this.archive.on(PackageCreatorEvent.ERROR, listener);
  }

  warning(listener: (error: Error) => void): void {
    this.archive.on(PackageCreatorEvent.WARNING, listener);
  }

  data(listener: (data: Buffer) => void): void {
    this.archive.on(PackageCreatorEvent.DATA, listener);
  }

  progress(listener: (progress: ProgressData) => void): void {
    this.archive.on(PackageCreatorEvent.PROGRESS, listener);
  }

  close(listener: () => void): void {
    this.archive.on(PackageCreatorEvent.CLOSE, listener);
  }

  finish(listener: () => void): void {
    this.archive.on(PackageCreatorEvent.FINISH, listener);
  }
}

export class ArchiverService implements IArchiver {
  private archive: Archiver;
  private readonly scope: ArchiverServiceScopedEvents;

  constructor(type: ArchiveFormat, compressionLevel: number) {
    this.archive = archiver(type, { zlib: { level: compressionLevel } });
  }

  public get stream() {
    return this.archive;
  }

  async finalize(): Promise<void> {
    await this.archive.finalize();
  }

  append({ stream, name }: File): void {
    this.archive.append(stream, { name });
  }

  directory(sourcePath: string, destinationPath: string): void {
    this.archive.directory(sourcePath, destinationPath);
  }

  pipe(stream: stream.Writable): void {
    this.archive.pipe(stream);
  }

  appendAll(...files: File[]): void {
    files.forEach((file) => this.append(file));
  }

  on(): ArchiverServiceScopedEvents {
    return this.scope || new ArchiverServiceScopedEvents(this.archive);
  }
}

export class PackageCreatorService {
  static generateArchive(type: ArchiveFormat, compressionLevel = 0): IArchiver {
    return new ArchiverService(type, compressionLevel);
  }
}
