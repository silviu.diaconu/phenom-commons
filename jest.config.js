module.exports = {
  projects: [
    '<rootDir>/libs/queue-service',
    '<rootDir>/libs/nest-queue-service',
    '<rootDir>/libs/package-creator',
    '<rootDir>/libs/xml-generator',
    '<rootDir>/libs/test-utils',
    '<rootDir>/libs/emails',
    '<rootDir>/libs/file-repository',
    '<rootDir>/libs/logger',
  ],
};
